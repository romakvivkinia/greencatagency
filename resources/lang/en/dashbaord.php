<?php

return [
    "dashboard_item_saved"            => "The order was successfully executed",
    "dashboard_item_updateded"        => "The order was successfully executed",
    "dashboard_item_deleted"          => "The order was successfully executed",
    "dashboard_chooce_item"           => "Choose",
    "dashboard_modal_title"           => "Message",
    "dashboard_modal_text"            => "Are you sure you want to perform this operation??",
    "dashboard_modal_yes"             => "Yes",
    "dashboard_modal_no"              => "No",
    "dashboard_item_traslate_message" => "Translate data :from -From   :to -to",
    
];