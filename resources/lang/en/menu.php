<?php


return [

    "dashboard_item_save"            => "Save",
    "dashboard_item_create"          => "Create",
    "dashboard_item_edit"            => "Edit",
    "dashboard_item_list"            => "List",
    "dashboard_main_nav_label"       => "Navigation",
    "dashboard_settings_label"       => "Settings",
    "dashboard_logout_label"         => "Logout",
    "dashboard_language_label"       => "Language",
    
    
    "dashboard_agreed_order"         => "Done",
    "dashboard_not_agreed_order"     => "Current",


    "dashboard_index"                => "Main",
    "dashboard_slider"               =>"Slider",
    "dashboard_file_manager"         => "File manager",
    "dashboard_menu_meta"            => "Meta",
    "dashboard_menu_og_meta"         => "OG meta",



];