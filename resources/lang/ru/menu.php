<?php


return [
    "dashboard_item_save"            => "Сохранить",
    "dashboard_item_create"          => "Создать",
    "dashboard_item_edit"            => "Редактировать",
    "dashboard_item_list"            => "Список",
    "dashboard_main_nav_label"       => "Навигация",
    "dashboard_settings_label"       => "Настройки",
    "dashboard_logout_label"         => "Выйти",
    "dashboard_language_label"       => "Язык",
    "dashboard_agreed_statement"     => "Подтверждено",
    "dashboard_not_agreed_statement" => "Неподтвержденный",
    "dashboard_agreed_order"         => "Завершенный",
    "dashboard_not_agreed_order"     => "Текущий",


    //menu
    "dashboard_index"                => "Главная",
    "dashboard_slider"               =>"სლაიდერი",
    "dashboard_file_manager"         => "Файловый менеджер",
    "dashboard_menu_meta"            => "Meta",
    "dashboard_menu_og_meta"         => "OG meta",

];