<?php

return [
    "dashboard_item_saved"            => "Заказ был успешно выполнен",
    "dashboard_item_updateded"        => "Заказ был успешно выполнен",
    "dashboard_item_deleted"          => "Заказ был успешно выполнен",
    "dashboard_chooce_item"           => "Выберите",
    "dashboard_modal_title"           => "Сообщение",
    "dashboard_modal_text"            => "Вы уверены, что хотите выполнить эту операцию??",
    "dashboard_modal_yes"             => "Да",
    "dashboard_modal_no"              => "Нет",
    "dashboard_item_traslate_message" => "Перевести данные :from -от   :to -на",
    
];