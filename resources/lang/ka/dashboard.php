<?php

return [
    "dashboard_item_saved"            => "ბრძანება წარმატებით შესრულდა",
    "dashboard_item_updateded"        => "ბრძანება წარმატებით შესრულდა",
    "dashboard_item_deleted"          => "ბრძანება წარმატებით შესრულდა",
    "dashboard_chooce_item"           => "აირჩიეთ",
    "dashboard_modal_title"           => "შეტყობინება",
    "dashboard_modal_text"            => "დარწმუნებული ხართ რომ გსურთ ამ ოპერაციის შესრულება?",
    "dashboard_modal_yes"             => "დიახ",
    "dashboard_modal_no"              => "არა",
    "dashboard_item_traslate_message" => "თარგმნეთ მონაცემები :from -დან   :to -ზე",


    "dashboard_first_name_label"      => "სახელი",
    "dashboard_last_name_label"       => "გვარი",
    "dashboard_email_label"           => "ელ.ფოსტა",
    "dashboard_role_label"            => "როლი",
    "dashboard_ln_label"              => "Linkedin",
    "dashboard_fb_label"              => "Facebook",
    "dashboard_gm_label"              => "Gmail",
    "dashboard_tw_label"              => "Tweeter",
    "dashboard_password_label"         => "პაროლი",
    "dashboard_possition_label"           => "თანამდებობა",
    "dashboard_avatar_title"           => "სუართი",
    "dashboard_bio_title"           => "ბიოგრაფია",
    
];