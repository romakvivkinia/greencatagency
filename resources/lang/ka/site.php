<?php

return  [
    //
    "menu_home"=>"მთავარი",
    "menu_about_us"=>"ჩევსნ შესახებ",
    "menu_gallery"=>"გალერეა",
    "menu_blog"=>"ბლოგი",
    "menu_contact"=>"კონტაქტი",


    "contact_section_need_support" => "გჭირდებათ დახმარება?",
    "contact_section_contcat_us" => "დაგვიკავშირდით",
    "contact_section_submit_btn" => "გაგზავნა",

    "processes_section_our_process" => "ჩვენი პროცესები",

    "specialization_section_our_specialization" => "ჩვენი სპეციალიზაცია",

    "team_section_our_team" => "ჩვენი გუნდი",

    "news_section_latest_news" => "ბოლო სიახლეები",
    "news_section_all_news" => "ყველა სიახლე",
];