<?php


return  [


    "dashboard_item_save"            => "შენახვა",
    "dashboard_item_create"          => "შექმნა",
    "dashboard_item_edit"            => "რედაქტირება",
    "dashboard_item_list"            => "სია",
    "dashboard_main_nav_label"       => "ნავიგაცია",
    "dashboard_settings_label"       => "პარამეტრები",
    "dashboard_logout_label"         => "გამოსვლა",
    "dashboard_language_label"       => "ენა",
    "dashboard_agreed_statement"     => "დადასტურებული",
    "dashboard_not_agreed_statement" => "დაუდასტურებელი",
    "dashboard_agreed_order"         => "შესრულებული",
    "dashboard_not_agreed_order"     => "მიმდინარე",
    "dashboard_back_to_list"    => "სია",


    //menu
    "dashboard_index"                => "მთავარი",
    "dashboard_slider"               =>"სლაიდერი",
    "dashboard_proces"               => "პროცესები",
    "dashboard_specialization"       => "სპეციალიზაცია",
    "dashboard_users"                => "მომხარებლები",
    "dashboard_file_manager"         => "ფაილ მენეჯერი",
    "dashboard_menu_meta"            => "Meta",
    "dashboard_menu_og_meta"         => "OG meta",
];