<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard - Green Cat Agency</title>

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ app('meta_og')->image ? asset(app('meta_og')->image) : asset('assets/dashboard/dist/img/ico/favicon.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('assets/dashboard/dist/img/ico/apple-touch-icon-57-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('assets/dashboard/dist/img/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('assets/dashboard/dist/img/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('assets/dashboard/dist/img/ico/apple-touch-icon-144-precomposed.png')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Start Global Mandatory Style
    =====================================================================-->
    <!-- jquery-ui css -->
    <link href="{{asset('assets/dashboard/plugins/jquery-ui-1.12.1/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap -->
    <link href="{{asset('assets/dashboard/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap rtl -->
    <!--<link href="{{asset('assets/dashboard')}}/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
    <!-- Lobipanel css -->
    <link href="{{asset('assets/dashboard/plugins/lobipanel/lobipanel.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Pace css -->
    <link href="{{asset('assets/dashboard/plugins/pace/flash.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome -->
    <link href="{{asset('assets/dashboard/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Pe-icon -->
    <link href="{{asset('assets/dashboard/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Themify icons -->
    <link href="{{asset('assets/dashboard/themify-icons/themify-icons.css')}}" rel="stylesheet" type="text/css"/>
    <!-- End Global Mandatory Style
    =====================================================================-->
    <!-- Start Theme Layout Style
    =====================================================================-->
    <!-- Theme style -->
    <link href="{{asset('assets/dashboard/plugins/colorbox/css/colorbox.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/dashboard/dist/css/styleBD.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/dashboard/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Theme style rtl -->
    <!--<link href="{{asset('assets/dashboard/dist/css/styleBD-rtl.css')}}" rel="stylesheet" type="text/css"/>-->
    <!-- End Theme Layout Style
    =====================================================================-->
    @stack("style")
</head>

<body class="hold-transition sidebar-mini ">

<!-- Site wrapper -->
<div class="wrapper">
    @include("dashboard.inc.header")
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    @include("dashboard.inc.menu")
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    @yield("content")
    <!-- /.content-wrapper -->


    @include("dashboard.inc.footer")
</div> <!-- ./wrapper -->
<!-- Start Core Plugins
=====================================================================-->
<!-- jQuery -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script> -->
<script src="{{asset('assets/dashboard/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- jquery-ui -->
<script src="{{asset('assets/dashboard/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{asset('assets/dashboard/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<!-- lobipanel -->
<script src="{{asset('assets/dashboard/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
<!-- Pace js -->
<script src="{{asset('assets/dashboard/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{asset('assets/dashboard/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{asset('assets/dashboard/plugins/fastclick/fastclick.min.js')}}" type="text/javascript"></script>
<!-- AdminBD frame -->
<script src="{{asset('assets/dashboard/dist/js/frame.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/dashboard/plugins/colorbox/jquery.colorbox-min.js')}}" type="text/javascript"></script>
<!-- End Core Plugins
=====================================================================-->
<!-- Start Theme label Script
=====================================================================-->
<!-- Dashboard js -->
<script src="{{asset('assets/dashboard/dist/js/dashboard.js')}}" type="text/javascript"></script>
<!-- End Theme label Script
=====================================================================-->
<script type="text/javascript" src="{{asset('packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>
<script src="{{asset('assets/dashboard/plugins/sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>

<script>
    $(function(){
        $($("ul.dropdown-menu.dropdown-menu-right a")[2]).on('click',function(){
            window.location = self.location;
        })
    })
</script>
@stack("scripts")
</body>


</html>