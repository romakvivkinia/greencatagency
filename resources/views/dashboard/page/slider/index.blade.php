@extends("dashboard.layout.app")
@push("style")
    <link rel="stylesheet" href="{{asset('assets/dashboard/plugins/datatables/dataTables.min.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">

@endpush

@section("content")
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon"><i class="pe-7s-close"></i></div>
        <div class="header-title">
            <h1>{{__("menu.dashboard_item_list")}}</h1>
            <small>&nbsp;</small>
            <ol class="breadcrumb">
            <li><a href="{{route('dashboard')}}">{{__("menu.dashboard_index")}}</a></li>
                <li class="active">{{__("menu.dashboard_item_list")}}</li>
                <li><a href="{{route('slider.create')}}">{{__("menu.dashboard_item_create")}}</a></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>&nbsp;</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                       <div class="row">
                           <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="cities" class="table table-bordered table-striped table-hover" >
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>{{__("dashboard.dashboard_active_label")}}</th>
                                            <th>{{__("dashboard.dashboard_title_label")}}</th>
                                            <th>{{__("dashboard.dashboard_image_label")}}</th>
                                            <th><i class="fa fa-cogs"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>

                           </div>
                       </div>
                    </div>
                    <div class="panel-footer">
                        This is standard panel footer
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.content -->
</div>
@endsection


@push('scripts')
    <script src="{{asset('assets/dashboard/plugins/datatables/dataTables.min.js')}}"></script>
             <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>

    <script>
        $(function () {
            var table = $("#cities").DataTable({
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'meetings', className: 'btn-sm'},
                    {extend: 'excel', title: 'meetings', className: 'btn-sm'},
                    {extend: 'print', className: 'btn-sm'}
                ],
                "columnDefs" : [
                    { "orderable": false, "targets": [4] },
                    { "orderable": true, "targets": [0,1,2,3] }
                ],
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "searching": true,
                "autoFill" : true,
                "dataSrc": "tableData",
                "order": [[ 0, "desc" ]],
                "pageLength": 20,
                "ajax": {
                    "url": "{{route("slider.index")}}",
                    "type": "GET",
                  
                    "dataSrc": function(json){
                        json.data = json.data.map(function(item,index){
                            item.active = item.active ? '<i class="fa fa-check text-success" aria-hidden="true"></i>' : ''

                            item.cogs = '<button ' +
                                            'class="btn btn-xs btn-info  edit_item"'+
                                            'data-item='+(item.item_id)+ 
                                            '><i class="fa fa-pencil"></i></button> '+
                                            '<button '+ 
                                                    'class="btn btn-xs btn-danger delete_item"'+
                                                    'data-item='+(item.id)+    
                                                    '><i class="fa fa-trash"></i></button>';
                          
                            return item;
                        });
                        return json.data;
                    },

                },

                "columns": [
                    { data: "id" },
                    { data: "active" },
                    { data: "title" },
                    { data: "image" },
                    { data: "cogs" }
                ]





            }).on("draw",function(){

            $('[name="cities_length"]').select2({
                    minimumResultsForSearch: -1
                }); 

                //redirect to edit page
                $("button.edit_item").on('click',function(e){
                    e.preventDefault();
                    var obj = $(this);
                    var url = "{{route('slider.edit',[':user_id'])}}";
                    url = url.replace(":user_id",obj.data('item'));

                    window.location.href = url;

                });

                //delete user 

                $("button.delete_item").on('click',function(e){
                    e.preventDefault();
                    var obj = $(this);
                    var url = "{{route('slider.destroy',[':user_id'])}}";
                    url     = url.replace(":user_id",obj.data('item'));
                    swal({
                            title: "{{__('dashboard.dashboard_modal_title')}}",
                            text: "{{__('dashboard.dashboard_modal_text')}}",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "{{__('dashboard.dashboard_modal_yes')}}",
                            cancelButtonText: "{{__('dashboard.dashboard_modal_no')}}",
                            closeOnConfirm: false
                        },
                        function(){
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                method:"DELETE",
                                url:url,
                                dataType:"JSON"
                            }).done(function(res){
                                table.ajax.reload();
                                swal(
                                    "{{__('dashboard.dashboard_modal_title')}}",
                                    "{{__('dashboard.dashboard_item_updateded')}}",
                                    "success"
                                );
                            }).fail(function(err){
                                swal(
                                    "{{__('dashboard.dashboard_modal_title')}}",
                                    "Error",
                                    "error"
                                );
                            });
                            
                    });
                })
                
            });
        });
    </script>
@endpush        
