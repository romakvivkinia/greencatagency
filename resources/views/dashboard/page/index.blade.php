@extends("dashboard.layout.app")
@section("content")
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon"><i class="pe-7s-close"></i></div>
        <div class="header-title">
            <h1>Blank page</h1>
            <small>it all starts here</small>
            <ol class="breadcrumb">
                <li><a href="index.html">This is</a></li>
                <li class="active">Breadcrumb</li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>This is page content</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <p>You can create here any grid layout you want. And any variation layout you imagine:) Check out main dashboard and other site. It use many different layout. </p>
                    </div>
                    <div class="panel-footer">
                        This is standard panel footer
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.content -->
</div>
@endsection