@include("dashboard.block.form_messages")
@push('style')
<link href="{{asset('assets/dashboard/plugins/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
@endpush
<br>
<div class="row">
    <div class="col-md-12"><button class="btn btn-success pull-right" type="submit">{{__("menu.dashboard_item_save")}}</button></div>
</div>
<br>

<div class="row">
    
    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_first_name_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-file-o"></i></div>
                <input type="text" class="form-control"
                    name="first_name"
                    value="{{  old('first_name') ? old('first_name') : (!empty($item) ? $item->first_name : '' ) }}"
                >
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_last_name_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-file-o"></i></div>
                <input type="text" class="form-control"
                    name="last_name"
                    value="{{  old('last_name') ? old('last_name') : (!empty($item) ? $item->last_name : '' ) }}"
                >
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_email_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                <input type="text" class="form-control"
                    name="email"
                    value="{{  old('email') ? old('email') : (!empty($item) ? $item->email : '' ) }}"
                >
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_role_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user-secret"></i></div>
                <select 
                    name="role_id" 
                    class="form-control" 
                    value="{{old('role_id') ? old('role_id') :(!empty($item) ? $item->role_id : '' ) }}"
                    >
                    @if(!empty($roles) && $roles->isNotEmpty())
                        <option value="">{{__("dashboard.dashboard_chooce_item")}}</option>
                        @foreach($roles as $role)
                            <option value="{{$role->item_id}}"
                            {{old('role_id') && old('role_id')==$role->item_id ? 'selected' :(!empty($item) && $item->role_id==$role->item_id  ? 'selected'  : '' ) }}
                            >{{$role->title}}</option>
                        @endforeach
                    @endif
                    
                </select>
            </div>
        </div>
    </div>
    

   
</div>


<br>

<div class="row">
    
    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_ln_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-file-o"></i></div>
                <input type="text" class="form-control"
                    name="ln"
                    value="{{  old('ln') ? old('ln') : (!empty($item) ? $item->ln : '' ) }}"
                >
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_fb_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-file-o"></i></div>
                <input type="text" class="form-control"
                    name="fb"
                    value="{{  old('fb') ? old('fb') : (!empty($item) ? $item->fb : '' ) }}"
                >
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_gm_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-file-o"></i></div>
                <input type="text" class="form-control"
                    name="gm"
                    value="{{  old('gm') ? old('gm') : (!empty($item) ? $item->gm : '' ) }}"
                >
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_tw_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-file-o"></i></div>
                <input type="text" class="form-control"
                    name="tw"
                    value="{{  old('tw') ? old('tw') : (!empty($item) ? $item->tw : '' ) }}"
                >
            </div>
        </div>
    </div>

</div>


<br>


<div class="row">
   
    <div class="col-md-6">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_password_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-key"></i></div>
                <input 
                    type="password" 
                    class="form-control" 
                    name="password" 
                    value="{{old('password') ? old('password') : '' }}"
                />  
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_possition_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                <input 
                    type="text" 
                    class="form-control" 
                    name="possition" 
                    value="{{  old('possition') ? old('possition') : (!empty($item) ? $item->possition : '' ) }}"
                />  
            </div>
        </div>
    </div>

</div>


<div class="row">

    

</div>
<div class="row">

<div class="col-md-6 ">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_avatar_title")}}</label>
            <div class="input-group">
            <input type="file" name="avatar" class="dropify form-control"  data-default-file="{{(!empty($item) && $item->avatar  ? asset($item->avatar) : '' )}}"  />
            </div>
            
        </div>
    </div>

    <div class="col-md-6 ">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_bio_title")}}</label>
            <div class="input-group">
                <textarea type="text" class="form-control"
                        name="bio"
                        rows="9" 
                        cols="50"
                >
                    {{  old('bio') ? old('bio') : (!empty($item) ? $item->bio : '' ) }}
                </textarea>
            </div>
            
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12"><button class="btn btn-success pull-right" type="submit">{{__("menu.dashboard_item_save")}}</button></div>
</div>

@push("scripts")

    <script type="text/javascript" src="{{asset("assets/dashboard/plugins/dropify/js/dropify.min.js")}}"></script>
    <script>
    $(function(){
        $('.dropify').dropify();
    })
    </script>

@endpush