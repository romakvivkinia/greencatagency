
@include("dashboard.block.form_messages")
<div class="row">
    <div class="col-md-8">
        @if($contact->item_id==1)
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Title</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-font"></i></div>
                        <input type="text" class="form-control" name="title" value="{{$contact->title}}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Analitic ID</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-google-plus"></i></div>
                        <input type="text" class="form-control" name="google" value="{{$contact->google}}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Facebook ID</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-facebook-square"></i></div>
                        <input type="text" class="form-control" name="facebook" value="{{$contact->facebook}}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Adress</label>
                    <div class="input-group div input-group">
                        <div class="input-group-addon"><i class="fa fa-map"></i></div>
                        <input type="text" class="form-control" name="address" value="{{$contact->address}}">
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Keywords</label>
                    <textarea name="keywords" id="" cols="30" rows="10" class="form-control">{{$contact->keywords}}</textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="description" id="" cols="30" rows="10" class="form-control">{{$contact->description}}</textarea>
                </div>
            </div>
        </div>
        @elseif($contact->item_id==4)
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Facebook link</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-facebook-square"></i></div>
                            <input type="text" class="form-control" name="fb" value="{{$contact->fb}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Instagram</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-instagram"></i></div>
                            <input type="text" class="form-control" name="tw" value="{{$contact->tw}}">
                        </div>
                    </div>
                </div>
                <!--div class="col-md-4">
                    <div class="form-group">
                        <label for="">Linkedin Link</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-linkedin-square"></i></div>
                            <input type="text" class="form-control" name="ln" value="{{$contact->ln}}">
                        </div>
                    </div>
                </div-->
               
            </div>
            
            <div class="row">
               
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Phone </label>
                        <div class="input-group div input-group">
                            <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                            <input type="text" class="form-control" name="phone" value="{{$contact->phone}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Email</label>
                        <div class="input-group div input-group">
                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                            <input type="text" class="form-control" name="email" value="{{$contact->email}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Adress</label>
                        <div class="input-group div input-group">
                            <div class="input-group-addon"><i class="fa fa-map"></i></div>
                            <input type="text" class="form-control" name="address" value="{{$contact->address}}">
                        </div>
                    </div>
                </div>
                
            </div>
        @endif







    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="">{{$contact->id==1 ? 'Favicon 16x16' : 'Og Image 1200x1200'}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-picture-o"></i></div>
                <input type="text" class="form-control" name="image"  id="feature_image"  placeholder="{{__("dashboard.dashboard_input_image")}}" value="{{old("image") ? old("image") : (!empty($contact) ? $contact->image : "")}}">
                <div class="input-group-addon"><a href="javascript:void(0)" class="popup_selector" data-inputid="feature_image"><i class="fa fa-cloud-upload"></i></a></div>
            </div>
        </div>
    </div>
</div>



@push("scripts")

    <script type="text/javascript" src="{{asset("packages/barryvdh/elfinder/js/standalonepopup.min.js")}}"></script>

@endpush