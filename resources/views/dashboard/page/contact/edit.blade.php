@extends("dashboard.layout.app")

@section("content")

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="header-icon"><i class="pe-7s-close"></i></div>
            <div class="header-title">
                <h1>{{__("menu.dashboard_item_create")}}</h1>
                <small>&nbsp;</small>
                <ol class="breadcrumb">
                    <li><a href="{{route('dashboard')}}">{{__("menu.dashboard_index")}}</a></li>
                    <li class="active">{{__("menu.dashboard_item_create")}}</li>
                </ol>
            </div>
        </section>
        <!-- Main content -->
        <div class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>&nbsp;</h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form action="{{route('contact.update',['contact'=>$contact->item_id])}}" method="post">
                                <input type="hidden" name="_method" value="PUT">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success pull-right">{{__("menu.dashboard_item_save")}}</button>
                                    </div>
                                </div>
                                <br>
                                @include('dashboard.page.contact.form')
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success pull-right">{{__("menu.dashboard_item_save")}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div> <!-- /.content -->
    </div>

@endsection