<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from admin.bdtask.com/BdtaskAdmin_v1.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 11 Mar 2017 19:18:11 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dashboard</title>

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('assets/dashboard/dist/img/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('assets/dashboard/dist/img/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('assets/dashboard/dist/img/ico/apple-touch-icon-144-precomposed.png')}}">

    <!-- Bootstrap -->
    <link href="{{asset('assets/dashboard/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap rtl -->
    <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
    <!-- Pe-icon-7-stroke -->
    <link href="{{asset('assets/dashboard/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
    <!-- style css -->
    <link href="{{asset('assets/dashboard/dist/css/styleBD.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Theme style rtl -->
    <!--<link href="assets/dist/css/styleBD-rtl.css" rel="stylesheet" type="text/css"/>-->
</head>
<body>
<!-- Content Wrapper -->
<div class="login-wrapper">
    <div class="back-link">
        <a href="{{route('home')}}" class="btn btn-success">Back to Home</a>
    </div>
    <div class="container-center">
        <div class="panel panel-bd">
            <div class="panel-heading">
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe-7s-unlock"></i>
                    </div>
                    <div class="header-title">
                        <h3>Login</h3>
                        <small><strong>Please enter your credentials to login.</strong></small>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" id="loginForm" novalidate>
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="username">Email </label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        <span class="help-block small">Your unique username to app</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Password</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        <span class="help-block small">Your strong password</span>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary pull-right">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.content-wrapper -->
<!-- jQuery -->
<script src="{{asset('assets/dashboard/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- bootstrap js -->
<script src="{{asset('assets/dashboard/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
</body>

<!-- Mirrored from admin.bdtask.com/BdtaskAdmin_v1.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 11 Mar 2017 19:18:11 GMT -->
</html>