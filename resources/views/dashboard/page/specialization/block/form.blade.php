@include("dashboard.block.form_messages")
<br>
<div class="row">
    <div class="col-md-12"><button class="btn btn-success pull-right" type="submit">{{__("menu.dashboard_item_save")}}</button></div>
</div>
<br>
<div class="row">
<div class="col-md-3">
        <div class="form-group">
            <div class="checkbox checkbox-success checkbox-inline">
                <input type="checkbox" name="active" id="active" value="1" {{!empty($item) && $item->active ? 'checked' : ''}}>
                <label for="active"> {{__("dashboard.dashboard_active")}}</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    
    <div class="col-md-6">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_title_label")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-file-o"></i></div>
                <input type="text" class="form-control"
                    name="title"
                    value="{{  old('title') ? old('title') : (!empty($item) ? $item->title : '' ) }}"
                >
            </div>
            
        </div>
    </div>
    

    <div class="col-md-6">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_link_title")}}</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-picture-o"></i></div>
                <input type="text" class="form-control" readonly name="image"  id="feature_image"  placeholder="{{__("dashboard.dashboard_input_image")}}" value="{{old("image") ? old("image") : (!empty($item) ? $item->image : "")}}">
                <div class="input-group-addon"><a href="javascript:void(0)" class="popup_selector" data-inputid="feature_image"><i class="fa fa-cloud-upload"></i></a></div>
            </div>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
            <label for="">{{__("dashboard.dashboard_description_title")}}</label>
            <div class="input-group">
                <textarea type="text" class="form-control"
                        name="description"
                        rows="5" 
                        cols="50"
                >
                    {{  old('description') ? old('description') : (!empty($item) ? $item->description : '' ) }}
                </textarea>
            </div>
            
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12"><button class="btn btn-success pull-right" type="submit">{{__("menu.dashboard_item_save")}}</button></div>
</div>

@push("scripts")

    <script type="text/javascript" src="{{asset("packages/barryvdh/elfinder/js/standalonepopup.min.js")}}"></script>

@endpush