@extends("dashboard.layout.app")
@section("content")
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon"><i class="pe-7s-close"></i></div>
        <div class="header-title">
            <h1>{{__("menu.dashboard_item_create")}}</h1>
            <small>&nbsp;</small>
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard')}}">{{__("menu.dashboard_index")}}</a></li>
                <li class="active">{{__("menu.dashboard_item_create")}}</li>
                <li><a href="{{route('specialization.index')}}">{{__("menu.dashboard_back_to_list")}}</a></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>&nbsp;</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="{{route('specialization.store')}}" method="POST">
                            @csrf
                            @include("dashboard.page.specialization.block.form")
                        </form>
                        
                    </div>
                    <div class="panel-footer">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.content -->
</div>
@endsection