@if(!empty($item) && !is_null($item) && !empty($item->lang) && $item->lang !=app()->getLocale())
<div class="row">
        <div class="col-md-12"> 
            <div class="alert alert-info">
                <p class="txt-primary">
                    {{__("dashboard.dashboard_item_traslate_message",["from"=>$item->lang,"to"=>app()->getLocale()])}}
                    
                </p>
            </div>
        </div>    
</div>
@endif



@if ($errors->any())
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif


@if (session('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
               <h4>{{session("success")}}</h4>
            </div>
        </div>
    </div>
@endif

@push('scripts')
    <script>
        $("div.alert.alert-success").parent().parent().slideUp(3500);
    </script>
@endpush