<header class="main-header">
    <a href="" class="logo"> <!-- Logo -->
        <span class="logo-mini">
                        <!--<b>A</b>BD-->
                        <img src="{{asset('assets/site/img/logo.png')}}" alt="">
                    </span>
        <span class="logo-lg">
                        <!--<b>Admin</b>BD-->
                        <img src="{{asset('assets/site/img/logo.png')}}" alt="">
                    </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
            <span class="sr-only">Toggle navigation</span>
            <span class="pe-7s-keypad"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-language"></i>
                        <span class="label label-success">{{app()->getLocale()}}</span>
                    </a>
                    <ul class="dropdown-menu" style="height: 160px;">
                        <li class="header">{{__("menu.dashboard_language_label")}}</li>
                        <li>
                            <ul class="menu">

                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>
                                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                            {{ $properties['native'] }}
                                        </a>
                                    </li>
                                @endforeach


                            </ul>
                        </li>
                    </ul>


                <!-- settings -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="pe-7s-settings"></i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <form action="{{route('logout')}}" method="post" id="logaout">{{ csrf_field() }}</form>
                            <a href="javascript:void(0)" onclick="$('#logaout').submit()"><i class="pe-7s-key"></i> {{__("menu.dashboard_logout_label")}}</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>