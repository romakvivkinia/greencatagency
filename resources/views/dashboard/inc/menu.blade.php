<aside class="main-sidebar">
    <!-- sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel -->


        <!-- sidebar menu -->
        <ul class="sidebar-menu">
            <li class="header">{{__("menu.dashboard_main_nav_label")}}</li>
            <li>
                <a href="{{route("dashboard")}}"><i class="ti-home"></i> <span>{{__("menu.dashboard_index")}}</span>
                    <span class="pull-right-container">

                    </span>
                </a>
            </li>


            <li class="treeview">
                <a href="javascript:void(0)">
                    <i class="ti-layers"></i><span>{{__("menu.dashboard_slider")}}</span>
                    <span class="pull-right-container">
                        
                        <i class="fa fa-angle-left pull-right"></i>
                       
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('slider.create')}}">{{__("menu.dashboard_item_create")}}</a></li>
                    <li><a href="{{route('slider.index')}}">{{__("menu.dashboard_item_list")}}</a></li>

                </ul>
            </li>

            <li class="treeview">
                <a href="javascript:void(0)">
                    <i class="ti-microsoft"></i><span>{{__("menu.dashboard_proces")}}</span>
                    <span class="pull-right-container">
                        
                        <i class="fa fa-angle-left pull-right"></i>
                       
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('process.create')}}">{{__("menu.dashboard_item_create")}}</a></li>
                    <li><a href="{{route('process.index')}}">{{__("menu.dashboard_item_list")}}</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:void(0)">
                    <i class="ti-ruler-pencil"></i><span>{{__("menu.dashboard_specialization")}}</span>
                    <span class="pull-right-container">
                        
                        <i class="fa fa-angle-left pull-right"></i>
                       
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('specialization.create')}}">{{__("menu.dashboard_item_create")}}</a></li>
                    <li><a href="{{route('specialization.index')}}">{{__("menu.dashboard_item_list")}}</a></li>

                </ul>
            </li>

            <li class="treeview">
                <a href="javascript:void(0)">
                    <i class="fa fa-users"></i><span>{{__("menu.dashboard_users")}}</span>
                    <span class="pull-right-container">
                        
                        <i class="fa fa-angle-left pull-right"></i>
                       
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('user.create')}}">{{__("menu.dashboard_item_create")}}</a></li>
                    <li><a href="{{route('user.index')}}">{{__("menu.dashboard_item_list")}}</a></li>

                </ul>
            </li>

            <li class="header">{{__("menu.dashboard_settings_label")}}</li>

            <li class="treeview">
                <a href="javascript:void(0)">
                    <i class="fa fa-google-wallet"></i><span>{{__("menu.dashboard_menu_meta")}}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li><a href="{{route('contact.edit',[1])}}">{{__("menu.dashboard_menu_og_meta")}}</a></li>
                    <li><a href="{{route('contact.edit',[4])}}">{{__("menu.dashboard_menu_meta")}}</a></li>
                </ul>
            </li>
          

            <li>
                <a href="{{url("elfinder")}}"><i class="ti-direction-alt"></i> <span>{{__("menu.dashboard_file_manager")}}</span>
                    <span class="pull-right-container">

                    </span>
                </a>
            </li>


           




        </ul>
    </div> <!-- /.sidebar -->
</aside>