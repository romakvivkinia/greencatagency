<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">

    <!-- Title -->
    
    <title>{{ app('meta_og')->title }}</title>
    <link rel="shortcut icon" href="{{ app('meta_og')->image ? asset(app('meta_og')->image) : asset('assets/dashboard/dist/img/ico/favicon.png')}}" type="image/x-icon">

    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Plugins -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/animsition.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/jquery.bxslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/cubeportfolio.css')}}">

    <!-- Icon Fonts -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css\fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css\azzuro-icons.css')}}">

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/reset.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/shortcodes.css')}}">
    
    <!-- Favicon and Touch Icons -->
    
    <link rel="apple-touch-icon-precomposed" href="{{asset('assets/site/apple-touch-icon-precomposed.jpg')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @stack("style")
</head>

<body class="{{$sideBar ?  'right-sidebar' : 'no-sidebar page page '}} header-fixed">
    <div id="top"></div>
    <div id="wrapper">
        <div id="page" class="clearfix animsition">
           
           @include("site.inc.menu")

            

                   

                               
            @yield("content")


                     


              


        </div><!-- #page -->
    </div><!-- #wrapper -->

    <a id="scroll-top"></a>


<!-- Javascript -->
<script src="{{asset('assets/site/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/site/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/site/js/plugins.js')}}"></script>
<script src="{{asset('assets/site/js/animsition.js')}}"></script>
<script src="{{asset('assets/site/js/wow.min.js')}}"></script>
<script src="{{asset('assets/site/js/countto.js')}}"></script>
<script src="{{asset('assets/site/js/cubeportfolio.js')}}"></script>
<script src="{{asset('assets/site/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/site/js/equalize.min.js')}}"></script>
<script src="{{asset('assets/site/js/jquery.hoverdir.js')}}"></script>
<script src="{{asset('assets/site/js/modernizr.custom.97074.js')}}"></script>
<script src="{{asset('assets/site/js/magnific.popup.min.js')}}"></script>
<script src="{{asset('assets/site/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/site/js/validate.js')}}"></script>
<script src="{{asset('assets/site/js/jquery.bxslider.min.js')}}"></script>
<script src="{{asset('assets/site/js/main.js')}}"></script>
<script src="{{asset('assets/site/js/shortcodes.js')}}"></script>

@stack("scripts")
</body>
</html>