<div id="site-header-wrap">
    <header id="site-header" class="header-front-page style-1">
        <div id="site-header-inner" class="boxed-style-1">
            <div class="wrap-inner clearfix">          
                <div id="site-logo" class="clearfix">
                    <div id="site-logo-inner">
                        <a href="index-1.html" title="Primus" rel="home" class="main-logo">
                            <img src="{{ app('meta')->image ? asset(app('meta')->image) : asset('assets/site/imgs/logo.png') }}" alt="Primus" data-retina="{{ app('meta')->image ? asset(app('meta')->image) : asset('assets/site/imgs/logo.png') }}" data-width="158" data-height="37">
                        </a>
                    </div>
                </div><!-- #site-logo -->

                <div class="mobile-button"><span></span></div><!-- //mobile menu button -->

                <nav id="main-nav" class="main-nav">
                    <ul class="menu">
                    
                        <li class="{{\Route::currentRouteName() == 'home' ? 'current-menu-item' : ''}}"><a href="{{route('home')}}">{{__('site.menu_home')}}</a></li>
                        <li class="{{\Route::currentRouteName() == 'about' ? 'current-menu-item' : ''}}"><a href="{{route('about')}}">{{__('site.menu_about_us')}}</a></li>
                        <li class="{{\Route::currentRouteName() == 'gallery' ? 'current-menu-item' : ''}}"><a href="#">{{__('site.menu_gallery')}}</a></li>
                        <li class="{{\Route::currentRouteName() == 'articles' ? 'current-menu-item' : ''}}"><a href="{{route('articles')}}">{{__('site.menu_blog')}}</a></li>
                        <li class="{{\Route::currentRouteName() == 'contact' ? 'current-menu-item' : ''}}"><a href="#">{{__('site.menu_contact')}}</a></li>
                    </ul>								
                </nav><!-- #main-nav -->  
                

                <div class="socials clearfix">

                    @foreach(array_reverse(LaravelLocalization::getSupportedLocales()) as $localeCode => $properties)
                    
                            <div class="icon" >
                                <a style="{{ app()->getLocale() == $localeCode ? 'color: #719e54;': ''}}" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ strtoupper($localeCode) }}
                                </a>
                            </div>
                    @endforeach
                    
                </div>
            </div>
        </div><!-- #site-header-inner -->
    </header><!-- #site-header -->
</div><!-- #site-header-wrap -->