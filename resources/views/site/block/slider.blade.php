@if(!empty($items))
<div class="slider-container style-1">
    <div class="homeslide">
        <ul class="bxslider">

            @foreach($items as $item)
                <li class="slider-images " style="background:url('{{asset($item->image ? $item->image : 'assets/site/imgs/slider2.jpg')}}') center center;background-repeat: no-repeat;background-size: cover;">
                    <div class="container">
                        <div class="row">
                            <div class="homeslidecontent">
                                <h5>&nbsp;</h5>
                                <h2>{{$item->title}}</h2>
                                <div class="cbr-links style-2">
                                    <span class="simple-link"><a href="{{ $item->link ? $item->link : 'javascript:void(0)'}}" target="_blank">View Projects</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach

           
        </ul><!-- .bxslider -->

        <nav class="nav2slider">
            <div class="container">
                <div class="row">
                @php
                    $index = 1;
                @endphp
                @foreach($items as $key=>$item)
                    <div class="col-md-4 no-padding">
                        <div class="slider-nav">
                            <a href="#" data-slide-index="{{$key}}">
                                <div class="project-nav">
                                    <div class="number"><span>{{$index}}</span></div>
                                    <div class="title"><span>{{$item->title}}</span></div>
                                </div>
                            </a>
                        </div>
                    </div><!-- .col-md-4 -->
                    @php
                        $index +=1;
                    @endphp
                @endforeach    

                   

                </div><!-- .row -->
            </div><!-- .container -->
        </nav><!-- .nav2slider -->
    </div><!-- .homeslide -->
</div><!-- .slidercontainer -->

@endif