@if(!empty($items))
<div id="team" class="row-member-post">
    <div class="container"></br></br></br></br></br>
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-headings">
                    <h2 class="heading style-1">{{__('site.team_section_our_team')}}</h2>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="69" data-mobi="44" data-smobi="44"></div>
            </div><!-- .col-md-12 -->

            <div class="col-md-12">
                <div class="cbr-team style-1 has-arrows arrow-top arrow80" data-auto="false" data-column="3" data-column2="2" data-column3="1" data-gap="30">
                    <div class="owl-carousel owl-theme">

                    @foreach($items as $item)
                        <div class="animate-box wow fadeInUpSmall" data-wow-delay="0.4s" data-wow-duration="0.4s">
                            <div class="team-item clearfix">
                                <div class="inner">
                                    <div class="thumb">
                                        <img src="{{asset($item->avatar)}}" alt="Image">

                                        <div class="text-wrap">
                                            <p>{{$item->bio}}</p>
                                        </div>

                                        <ul class="socials clearfix">
                                            <li class="facebook"><a class="icon active" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                            <li class="twitter"><a class="icon" href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li class="facebook"><a class="icon" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li class="linkedin"><a class="icon" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                        </ul>
                                    </div><!-- .thumb -->

                                    <div class="information">
                                        <h4 class="name">{{$item->first_name}} {{$item->last_name}}</h4>
                                        <div class="position">{{$item->possition}}</div>
                                    </div>
                                </div>
                            </div><!-- .team-item -->
                        </div><!-- .animate-box -->
                    @endforeach    

                       


                    </div>
                </div><!-- .cbr-team -->

                <div class="cbr-spacer clearfix" data-desktop="132" data-mobi="92" data-smobi="92"></div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .row-member-post -->
@endif