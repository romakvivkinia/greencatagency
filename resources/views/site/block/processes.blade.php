@if(!empty($items))
<div class="row-process gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="130" data-mobi="90" data-smobi="90"></div>

                <div class="cbr-headings">
                    <h2 class="heading style-1">{{__('site.processes_section_our_process')}}</h2>
                </div>

                <div class="cbr-spacer clearfix" data-desktop="69" data-mobi="44" data-smobi="44"></div>
            </div><!-- .col-md-12 -->

            @php
                $index = 1;
            @endphp
            @foreach($items as $key=>$item)
            <div class="col-lg-4 col-md-6">
                <div class="animate-box wow fadeInLeftSmall" data-wow-delay="0.4s">
                    <div class="cbr-process style-1">
                        <div class="process-text">
                            <div class="text-wrap">
                                <div class="number"><span>{{'0'.$index}}</span></div>
                                <div class="heading"><h5>{{$item->title}}</h5></div>
                                <div class="desc"><span>{{$item->description}}</span></div>
                            </div>
                        </div>
                    </div><!-- .cbr-process -->
                </div><!-- .animate-box -->

                <div class="cbr-spacer clearfix" data-desktop="55" data-mobi="40" data-smobi="40"></div>
            </div><!-- .col-lg-4 -->
            @php
                $index +=1;
            @endphp
            @endforeach
   

        </div><!-- .row -->
    </div><!-- .container -->   
</div><!-- .row-process --> 
@endif