<article class="hentry">
    <div class="post-media clearfix">
        <a href="#"><img src="{{asset($item->image)}}" alt="Image"></a>
    </div><!-- .post-media -->

    <div class="post-content-wrap">
        <h2 class="post-title">
            <span>
                <a href="#">{{$item->title}}</a>
            </span>
        </h2><!-- .post-title -->

        <div class="post-meta">
            <div class="post-meta-content">
                <div class="post-meta-content-inner">
                    

                    <span class="post-date item">
                        <span class="inner"><span class="entry-date">{{\Carbon\Carbon::parse($item->published_at)->format('Y-m-d')}}</span></span>
                    </span>

                    <span class="post-meta-categories item">
                        <span class="inner">{{$item->category->title}}</span>
                    </span>
                </div>
            </div>
        </div><!-- .post-meta -->

        <div class="post-content post-excerpt">
            <p>&nbsp;</p>
        </div><!-- .post-excerpt -->

        <div class="post-read-more">
            <div class="post-link">
                <a href="#">Read more</a>
            </div>
        </div><!-- .post-read-more -->
    </div><!-- .post-content-wrap -->
</article><!-- .hentry -->