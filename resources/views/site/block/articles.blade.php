@if(!empty($items))
<div id="news" class="row-latest-news">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="130" data-mobi="90" data-smobi="90"></div>

                <div class="cbr-headings">
                    <h2 class="heading style-1">{{__('site.news_section_latest_news')}}</h2>
                    <span class="links"><a href="#">{{__('site.news_section_all_news')}}</a></span>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="70" data-mobi="59" data-smobi="59"></div>
            </div><!-- .col-md-12 -->

            <div class="col-md-12">
                <div class="cbr-carousel-box" data-auto="false" data-loop="false" data-gap="30" data-column="3" data-column2="2" data-column3="1">
                    <div class="owl-carousel owl-theme owl-loaded owl-drag">


                    @foreach($items as $item)
                        <div class="animate-box wow fadeInUpSmall" data-wow-delay="0.4s" data-wow-duration="0.4s">
                            <article class="hentry post-grid style-4">
                                <div class="post-media clearfix">
                                    <a href="#"><img src="{{asset($item->image)}}" alt="Image"></a>
                                </div><!-- .post-media -->

                                <div class="post-content-wrap">
                                    <h2 class="post-title">
                                        <span><a href="page-blog-single.html">{{$item->title}}</a></span>
                                    </h2><!-- .post-title -->
                                </div><!-- .post-content-wrap -->

                                <div class="post-meta">
                                    <div class="post-meta-content">
                                        <div class="post-meta-content-inner">
                                            <span class="post-date item">
                                                <span class="inner"><span class="entry-date">{{\Carbon\Carbon::parse($item->published_at)->format('Y-m-d')}}</span></span>
                                            </span>
                                            <span class="post-meta-categories item">
                                                <span class="inner"><a href="#">{{$item->category->title}}</a></span>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- .post-meta -->
                            </article><!-- .hentry -->
                        </div><!-- .animate-box -->
                    @endforeach    



                    </div>
                </div><!-- .post-related -->

                <div class="cbr-spacer clearfix" data-desktop="135" data-mobi="95" data-smobi="63"></div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .row-latest-news -->
@endif