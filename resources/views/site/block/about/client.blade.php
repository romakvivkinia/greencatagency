@if(!empty($items))
<div class="row-testimonials gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="130" data-mobi="90" data-smobi="90"></div>

                <div class="cbr-headings">
                    <h2 class="heading style-1">What Our Clients Say</h2>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="76" data-mobi="51" data-smobi="51"></div>
            </div><!-- .col-md-12 -->

            <div class="col-md-12">
                <div class="cbr-carousel-box has-arrows arrow-center offsetcenter-style-2 offset-v-30" data-auto="true" data-loop="true" data-gap="30" data-column="2" data-column2="1" data-column3="1">
                    <div class="owl-carousel owl-theme">
                        <div class="cbr-testimonials style-3 clearfix">
                            <div class="item">
                                <div class="inner">
                                    <blockquote>
                                        <div class="thumb">
                                            <img src="assets\imgs\testimonials\1.png" alt="Image">
                                        </div>

                                        <div class="information">
                                            <h6 class="name">Faye Hamilton</h6>
                                            <span class="position">CEO Plusstrip</span>
                                        </div>

                                        <p class="text">“We are looking forward to our project standing shoulder to shoulder with the others on your website, although we do think our flat is the best!”</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div><!-- .cbr-testimonials -->

                        <div class="cbr-testimonials style-3 clearfix">
                            <div class="item">
                                <div class="inner">
                                    <blockquote>
                                        <div class="thumb">
                                            <img src="assets\imgs\testimonials\2.png" alt="Image">
                                        </div>

                                        <div class="information">
                                            <h6 class="name">Jacqueline Guyon</h6>
                                            <span class="position">CMO Inity</span>
                                        </div>

                                        <p class="text">“For people. Preferably for you – and for all the things you find important in life. Every project we undertake is just as uniqueiinterpretation of your character.”</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div><!-- .cbr-testimonials -->

                        <div class="cbr-testimonials style-3 clearfix">
                            <div class="item">
                                <div class="inner">
                                    <blockquote>
                                        <div class="thumb">
                                            <img src="assets\imgs\testimonials\1.png" alt="Image">
                                        </div>

                                        <div class="information">
                                            <h6 class="name">Faye Hamilton</h6>
                                            <span class="position">CEO Plusstrip</span>
                                        </div>

                                        <p class="text">“We are looking forward to our project standing shoulder to shoulder with the others on your website, although we do think our flat is the best!”</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div><!-- .cbr-testimonials -->

                        <div class="cbr-testimonials style-3 clearfix">
                            <div class="item">
                                <div class="inner">
                                    <blockquote>
                                        <div class="thumb">
                                            <img src="assets\imgs\testimonials\2.png" alt="Image">
                                        </div>

                                        <div class="information">
                                            <h6 class="name">Jacqueline Guyon</h6>
                                            <span class="position">CMO Inity</span>
                                        </div>

                                        <p class="text">“For people. Preferably for you – and for all the things you find important in life. Every project we undertake is just as uniqueiinterpretation of your character.”</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div><!-- .cbr-testimonials -->

                        <div class="cbr-testimonials style-3 clearfix">
                            <div class="item">
                                <div class="inner">
                                    <blockquote>
                                        <div class="thumb">
                                            <img src="assets\imgs\testimonials\1.png" alt="Image">
                                        </div>

                                        <div class="information">
                                            <h6 class="name">Faye Hamilton</h6>
                                            <span class="position">CEO Plusstrip</span>
                                        </div>

                                        <p class="text">“We are looking forward to our project standing shoulder to shoulder with the others on your website, although we do think our flat is the best!”</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div><!-- .cbr-testimonials -->

                        <div class="cbr-testimonials style-3 clearfix">
                            <div class="item">
                                <div class="inner">
                                    <blockquote>
                                        <div class="thumb">
                                            <img src="assets\imgs\testimonials\2.png" alt="Image">
                                        </div>

                                        <div class="information">
                                            <h6 class="name">Jacqueline Guyon</h6>
                                            <span class="position">CMO Inity</span>
                                        </div>

                                        <p class="text">“For people. Preferably for you – and for all the things you find important in life. Every project we undertake is just as uniqueiinterpretation of your character.”</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div><!-- .cbr-testimonials -->
                    </div>
                </div><!-- .cbr-carousel-box -->
            </div><!-- .col-md-12 -->

            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="140" data-mobi="95" data-smobi="95"></div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .row-testimonials -->
@endif