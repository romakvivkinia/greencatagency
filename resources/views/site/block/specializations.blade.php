@if(!empty($items))
<div class="row-projects">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="130" data-mobi="84" data-smobi="84"></div>

                <div class="cbr-headings">
                    <h2 class="heading style-1">{{__('site.specialization_section_our_specialization')}}</h2>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="76" data-mobi="51" data-smobi="51"></div>
            </div><!-- .col-md-12 -->

            <div class="col-md-12">
                <div class="cbr-gallery has-arrows arrow-top arrow80" data-auto="true" data-loop="true" data-column="3" data-column2="2" data-column3="1" data-gap="30">
                    <div class="owl-carousel owl-theme">

                        @foreach($items as $item)
                            <div class="gallery-box style-6">
                                <div class="overlay">
                                    <img src="{{asset($item->image)}}" alt="Image">
                                    <div class="title">{{$item->title}}</div>

                                    <div class="hover-effect">
                                        <div class="text-wrap">
                                            <div class="categorie">{{$item->title}}</div>
                                            <div class="desc"><p>{{$item->description}}</p></div>
                                        </div>
                                    </div><!-- .hover-effect -->
                                </div>
                            </div><!-- .gallery-box -->
                        @endforeach

                       

                    </div></br></br></br></br></br></br>
                </div><!-- .cbr-gallery -->
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .row-projects -->
@endif