@push("style")
<link rel="stylesheet" type="text/css" href="{{asset('assets/dashboard/plugins/toastr/toastr.css')}}">
@endpush
<div id="contact" class="row-contact gray-bg boxed-style-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="80" data-mobi="50" data-smobi="50"></div>
            </div><!-- .col-md-12 -->

            <div class="col-md-5">
                <div class="cbr-headings">
                    <h2 class="heading style-1">{{__('site.contact_section_contcat_us')}}</h2>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="54" data-mobi="35" data-smobi="35"></div>

                <div class="cbr-headings">
                    <span class="sub-heading style-1">{{__('site.contact_section_need_support')}}</span>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="7" data-mobi="7" data-smobi="7"></div>

                <div class="cbr-contact-info">
                    <ul class="info-wrap">
                        <li class="address clearfix">
                            <div class="inner">
                                <div class="address"><span class="text">{{ app('meta_og')->address }}<br>{{ app('meta')->address }}</span></div>
                            </div>
                        </li>

                        <li class="phone-number clearfix">
                            <div class="inner">
                                <div class="phone"><span class="text">{{ app('meta')->tel }}</span></div>
                                <div class="phone"><span class="text">{{ app('meta')->phone }}</span></div>
                            </div>
                        </li>

                        <li class="email clearfix">
                            <div class="inner">
                                <div class="email"><span class="text">{{ app('meta')->emial }}</span></div>
                            </div>
                        </li>
                    </ul>
                </div><!-- .cbr-contact-info -->
            </div><!-- .col-md-5 -->

            <div class="col-md-7">
                <div class="cbr-spacer clearfix" data-desktop="90" data-mobi="34" data-smobi="34"></div>
                
                <div class="cbr-contact-form clearfix">
                    <form id="contact_us" action="{{route('send_contact')}}" method="post" class="contact-form wpcf7-form">
                        <div class="animate-box wow fadeInLeftSmall" data-wow-delay="0.4s" data-wow-duration="0.8s">
                            <span class="form-control-wrap your-name">
                                <input type="text" tabindex="1" id="name" name="name" value="" class="wpcf7-form-control" placeholder="Name" required="">
                            </span>
                            <span class="form-control-wrap your-email">
                                <input type="email" tabindex="2" id="email" name="email" value="" class="wpcf7-form-control" placeholder="E-mail" required="">
                            </span>
                            <span class="form-control-wrap your-phone">
                                <input type="text" tabindex="3" id="phone" name="phone" value="" class="wpcf7-form-control" placeholder="Phone">
                            </span>
                            <span class="form-control-wrap your-company">
                                <input type="text" tabindex="4" id="company" name="company" value="" class="wpcf7-form-control" placeholder="Company " required="">
                            </span>
                            <span class="form-control-wrap your-message">
                                <textarea name="message" tabindex="5" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" placeholder="Message" required=""></textarea>
                            </span>
                        </div><!-- .animate-box -->

                        <div class="animate-box wow fadeInUpSmall" data-wow-delay="0.6s" data-wow-duration="0.6s">
                            <div class="wrap-submit">
                                <input type="submit" value="{{__('site.contact_section_submit_btn')}}" class="submit big wpcf7-form-control wpcf7-submit">
                            </div>
                        </div><!-- .animate-box -->
                    </form>
                </div><!-- .cbr-contact-form -->
            </div><!-- .col-md-7 -->

            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="140" data-mobi="100" data-smobi="100"></div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .row-contact -->

@push('scripts')
<script src="{{asset('assets/dashboard/plugins/toastr/toastr.min.js')}}"></script>
<script>
$(function(){
    $("form#contact_us").on('submit',function(e){
        e.preventDefault();
        $.ajax({
            headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            type    : "POST",
            url     : $("form#contact_us").attr('action'),
            data    : $("form#contact_us").serialize(),
            dataType:"JSON",
        }).done(function(res){

            toastr.success(res.message);
            setTimeout(function(){
                window.location.href = self.location
            }, 3000);
            
                    
             
        }).fail(function(err){
            $.each(err.responseJSON.errors,function(index,obj){
                toastr.error(obj[0]);
            });
            
        })
    })
})
</script>
@endpush