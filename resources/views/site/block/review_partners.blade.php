@if(!empty($partners) || !empty($reviews))
<div id="testimonials" class="row-trusted parallax boxed-style-4 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="200" data-mobi="150" data-smobi="150"></div>

                <div class="animate-box wow fadeInRightSmall" data-wow-delay="0.2s">
                    <div class="cbr-testimonials style-1">
                        <div class="content-wrap">
                            @if(!empty($reviews))    
                                @foreach($reviews as $key=>$item)
                                    <div id="tab-{{$key+1}}" class="content">
                                        <blockquote>
                                            <div class="text-wrap"><p>{{$item->description}}</p></div>

                                            <div class="information">
                                                <cite class="name">{{$item->fullname}}</cite>
                                                <cite class="position">&nbsp;</cite>
                                            </div>
                                        </blockquote>
                                    </div>
                                @endforeach
                            @endif        

                           
                        </div>

                        <ul class="thumb">
                            @if(!empty($reviews))    
                                @foreach($reviews as $key=>$item)
                                    <li class="item {{$key==0 ? 'active' : ''}}"><img src="{{asset($item->image)}}" alt="Image"></li>
                                @endforeach
                            @endif  

                            
                            
                        </ul>
                    </div><!-- .cbr-testimonials -->
                </div><!-- .animate-box -->
            </div><!-- .col-md-12 -->

            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="160" data-mobi="85" data-smobi="85"></div>
                <div class="cbr-divider no-icon divider-center divider-solid-2"></div>
                <div class="clearfix"></div>
                <div class="cbr-spacer clearfix" data-desktop="45" data-mobi="35" data-smobi="35"></div>
            </div><!-- .col-md-12 -->

            <div class="col-md-12 no-padding">
                <div class="animate-box wow fadeInUpSmall" data-wow-delay="0.2s">
                    <div class="cbr-carousel-box owl-nav-disable owl-dots-disable" data-auto="false" data-loop="true" data-gap="68" data-column="5" data-column2="3" data-column3="2">
                        <div class="owl-carousel owl-theme">

                            @if(!empty($partners))
                                @foreach($partners as $item)
                                    <div class="cbr-adv-image">
                                        <div class="thumb">
                                            <a href="{{$item->link ? $item->link : 'javascript:void(0)'}}">
                                                <img src="{{asset($item->image)}}" alt="Image">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            
                        </div>
                    </div><!-- .cbr-carousel-box -->
                </div><!-- .animate-box -->

                <div class="cbr-spacer clearfix" data-desktop="45" data-mobi="35" data-smobi="35"></div>
            </div><!--col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .row-trusted -->
@endif