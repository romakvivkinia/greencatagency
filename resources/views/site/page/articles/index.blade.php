@extends("site.layout.app")


@section("content")


<div id="featured">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="featured-title" class="text-center">
                    <div id="breadcrumbs">
                        <div class="breadcrumb-trail">
                            <a href="#" title="Home" rel="home" class="trail-begin">Home</a> <span class="sep">/</span><span class="trail-end">Blog</span>
                        </div>
                    </div><!-- #breadcrumbs -->

                    <div class="featured-title-inner">
                        <div class="featured-title-heading-wrap">
                            <h1 class="featured-title-heading">Our Blog</h1>
                        </div>
                    </div><!-- .featured-title-inner -->
                </div><!-- #featured-title -->
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- #featured -->

<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="site-content" class="site-content clearfix">
                        <div id="inner-content" class="inner-content-wrap">

                            @if(!empty($items))

                                @foreach($items as $item)
                                    @include("site.block.articles.item")
                                @endforeach


                            <div class="cbr-pagination clearfix margin-top-70">
                                <ul class="page-numbers">
                                    <li><a class="prev page-numbers" href="#"><span class="fa fa-chevron-left"></span></a></li>
                                    <li><span class="page-numbers current">1</span></li>
                                    <li><a class="page-numbers" href="#">2</a></li>
                                    <li><a class="page-numbers" href="#">3</a></li>
                                    <li><a class="page-numbers" href="#">4</a></li>
                                    <li><a class="next page-numbers" href="#"><span class="fa fa-chevron-right"></span></a></li>
                                </ul>
                            </div><!-- .cbr-pagination -->
                            @endif
                        </div><!-- #inner-content -->
                    </div><!-- #site-content -->

                    <div id="sidebar" class="style-1">
                        <div id="inner-sidebar" class="inner-content-wrap">
                            <div class="widget widget_search">
                                <form role="search" method="get" action="#" class="search-form style-1">
                                    <input type="search" class="search-field" placeholder="Search" value="" name="s" title="Search for:">
                                    <button type="submit" class="search-submit" title="Search">Search</button>
                                </form>
                            </div><!-- .widget_search -->

                            <div class="widget widget_categories">
                                <h2 class="widget-title has-sep accent"><span>CATEGORIES</span></h2>

                                <ul>
                                    <li class="cat-item"><a href="#">All</a> 44</li>
                                    <li class="cat-item"><a href="#">Commercial</a> 5</li>
                                    <li class="cat-item"><a href="#">Architecture</a> 12</li>
                                    <li class="cat-item"><a href="#">Residential</a> 6</li>
                                    <li class="cat-item"><a href="#">Planning</a> 21</li>
                                </ul>
                            </div><!-- .widget_categories -->

                            <div class="widget widget_recent_news">
                                <h2 class="widget-title has-sep accent"><span>RECENT NEWS</span></h2>

                                <ul class="recent-news clearfix">
                                    <li class="clearfix">
                                        <div class="texts">
                                            <h3><a href="#">A Summer with the Big Blue in Santorini</a></h3>
                                            <span class="post-date"><span class="entry-date">25 Aug 2019</span></span>
                                            <span class="sep"><span>/</span></span>
                                            <span class="categories"><span class="entry-categories">Architecture</span></span>
                                        </div><!-- .texts -->
                                    </li>

                                    <li class="clearfix">
                                        <div class="texts">
                                            <h3><a href="#">New Library and Academy for Performing Arts</a></h3>
                                            <span class="post-date"><span class="entry-date">25 Aug 2019</span></span>
                                            <span class="sep"><span>/</span></span>
                                            <span class="categories"><span class="entry-categories">Architecture</span></span>
                                        </div><!-- .texts -->
                                    </li>

                                    <li class="clearfix">
                                        <div class="texts">
                                            <h3><a href="#">A Family House of Deceptive Simplicity in Bratislava</a></h3>
                                            <span class="post-date"><span class="entry-date">25 Aug 2019</span></span>
                                            <span class="sep"><span>/</span></span>
                                            <span class="categories"><span class="entry-categories">Architecture</span></span>
                                        </div><!-- .texts -->
                                    </li>
                                </ul>
                            </div><!-- .widget_recent_news -->

                            <div class="widget widget_archive">
                                <h2 class="widget-title has-sep accent"><span>ARCHIVES</span></h2>

                                <ul>
                                    <li class="archive-item"><a href="#">August 2019</a></li>
                                    <li class="archive-item"><a href="#">July 2019</a></li>
                                    <li class="archive-item"><a href="#">June 2019</a></li>
                                    <li class="archive-item"><a href="#">May 2019</a></li>
                                    <li class="archive-item"><a href="#">April 2019</a></li>
                                </ul>
                            </div><!-- .widget_archive -->

                            <div class="widget widget_tag_cloud">
                                <h2 class="widget-title has-sep accent"><span>TAGS CLOUD</span></h2>

                                <div class="tagcloud">
                                    <a href="#">Architecture</a>
                                    <a href="#">Commercial</a>
                                    <a href="#">Residential</a>
                                    <a href="#">House</a>
                                    <a href="#">Design</a>
                                    <a href="#">Apartment</a>
                                    <a href="#">Planning</a>
                                </div>
                            </div><!-- .widget_tag_cloud -->
                        </div>
                    </div><!-- #sidebar -->
                </div><!-- .col-md-12 -->
            </div><!-- .row -->
        </div><!-- .container -->
              
    </div><!-- #content-wrap -->
</div><!-- #main-content -->
@endsection
