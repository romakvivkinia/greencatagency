@extends("site.layout.app")


@section("content")


<div id="featured-image-1"></div><!-- #featured -->


<div class="row-about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="130" data-mobi="90" data-smobi="90"></div>
            </div><!-- .col-md-12 -->

            <div class="col-lg-5 col-md-12">
                <div class="cbr-headings">
                    <h2 class="heading style-1 wow fadeInUpSmall" data-wow-duration="0.6s" data-wow-delay="0">A well-designer interior expresses your values and life pholosophy.</h2>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="0" data-mobi="45" data-smobi="45"></div>
            </div><!-- .col-lg-5 -->

            <div class="col-lg-7 col-md-12">
                <div class="cbr-content-box clearfix" data-padding="" data-mobipadding="" data-margin="5px 0 0 100px" data-mobimargin="0 0 0 0">
                    <div class="inner">
                        <div class="cbr-text style-1">
                            <p class="text-wrap wow fadeInUpSmall" data-wow-duration="0.8s" data-wow-delay="0">Our long history of family ownership has provided financial reassurance, business continuity and stability. It has also created firm foundations from which to promote British design as we expand territories.</p>
                        </div><!-- .cbr-text -->

                        <div class="cbr-spacer clearfix" data-desktop="27" data-mobi="27" data-smobi="27"></div>

                        <div class="cbr-text style-1">
                            <p class="text-wrap wow fadeInUpSmall" data-wow-duration="1s" data-wow-delay="0">The basic philosophy of our studio is to create individual, aesthetically stunning solutions for our customers by lightning-fast development of projects employing unique styles and architecture. Even if you don't have a ready sketch of what you want - we will help you get the result you dreamed of.</p>
                        </div><!-- .cbr-text -->
                    </div>
                </div><!-- .cbr-content-box -->
            </div><!-- .col-lg-7 -->

            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="125" data-mobi="85" data-smobi="85"></div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div>

@include("site.block.specializations",["items"=>$specializations])

@include("site.block.about.client",["items"=>$reviews])


@include("site.block.team",["items"=>$team])




    

<div class="row-partners gray-bg">
    <div class="container">
    <div class="row">
        <div class="col-md-12 no-padding">
            </br></br></br></br>
            <div class="cbr-carousel-box owl-nav-disable owl-dots-disable" data-auto="false" data-loop="true" data-gap="73.33" data-column="4" data-column2="3" data-column3="2">
                <div class="owl-carousel owl-theme">

                    @foreach($partners as $item)
                    <div class="animate-box wow fadeInUpSmall" data-wow-delay="0.2s">
                        <div class="cbr-adv-image">
                            <div class="thumb">
                                <a href="#">
                                <img src="{{$item->image}}" alt="Image">
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                <!-- .animate-box -->
                


                </div>
            </div>
            <!-- .cbr-carousel-box -->
        </div>
        <!--col-md-12 -->
        <div class="col-md-12">
            <div class="cbr-spacer clearfix" data-desktop="140" data-mobi="100" data-smobi="100"></div>
        </div>
        <!-- .col-md-12 -->
        @include("site.block.contact")
        <!-- .row-contact -->
    </div>
    <!-- .row -->
    </div>
    <!-- .container-fluid -->
</div>
@endsection