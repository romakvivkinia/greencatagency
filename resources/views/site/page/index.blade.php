@extends("site.layout.app")


@section("content")
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">


@include("site.block.slider",["items"=>$sliders])

<div class="row-about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="130" data-mobi="90" data-smobi="90"></div>
            </div><!-- .col-md-12 -->

            <div class="col-lg-5 col-md-12">
                <div class="cbr-headings wow pulse" data-wow-duration="0.6s" data-wow-delay="0">
                    <h2 class="heading style-1">Delivering commercial success through inspirational design</h2>
                </div><!-- .cbr-headings -->

                <div class="cbr-spacer clearfix" data-desktop="44" data-mobi="44" data-smobi="44"></div>

                <div class="cbr-text style-1">
                    <p class="text-wrap wow pulse" data-wow-duration="1s" data-wow-delay="0">Our long history of family ownership has provided financial reassurance, business continuity and stability. It has also created firm foundations from which to promote British design as we expand territories.</p>
                </div><!-- .cbr-text -->

                <div class="cbr-spacer clearfix" data-desktop="66" data-mobi="38" data-smobi="38"></div>

                <div class="cbr-links style-3 wow fadeInRight" data-wow-duration="1.3s" data-wow-delay="0">
                    <span class="simple-link"><a href="#">About Us</a></span>
                </div>

                <div class="cbr-spacer clearfix" data-desktop="0" data-mobi="62" data-smobi="62"></div>
            </div><!-- .col-lg-5 -->

            <div class="col-lg-7 col-md-12">
                <div class="animate-box wow fadeInLeft" data-wow-duration="1.6s" data-wow-delay="0">
                    <div class="cbr-content-box clearfix" data-padding="" data-mobipadding="" data-margin="10px 0 0 70px" data-mobimargin="0 0 0 0">
                        <div class="inner">
                            <div class="cbr-gallery has-bullets bullet-style-2 has-arrows arrow-style-2 arrow-center offset-x text-center" data-auto="false" data-loop="false" data-gap="25" data-column="1" data-column2="1" data-column3="1">
                                <div class="owl-carousel owl-theme">
                                    <div class="gallery-box style-11">
                                        <div class="overlay image-left">
                                            <div class="thumb">
                                                <div class="thumb-wrap">
                                                    <img src="assets\imgs\about\about-1.jpg" alt="Image">
                                                    <div class="hover-effect"></div>
                                                    <a class="zoom-popup" href="assets\imgs\about\about-1.jpg"><span class="icon">+</span></a>
                                                </div>
                                            </div>
                                        </div><!-- .text-wrap -->
                                    </div><!-- .gallery-box -->

                                   <div class="gallery-box style-11">
                                        <div class="overlay image-left">
                                            <div class="thumb">
                                                <div class="thumb-wrap">
                                                    <img src="assets\imgs\about\about-1-2.jpg" alt="Image">
                                                    <div class="hover-effect"></div>
                                                    <a class="zoom-popup" href="assets\imgs\about\about-1-2.jpg"><span class="icon">+</span></a>
                                                </div>
                                            </div>
                                        </div><!-- .text-wrap -->
                                    </div><!-- .gallery-box -->

                                    <div class="gallery-box style-11">
                                        <div class="overlay image-left">
                                            <div class="thumb">
                                                <div class="thumb-wrap">
                                                    <img src="assets\imgs\about\about-1-3.jpg" alt="Image">
                                                    <div class="hover-effect"></div>
                                                    <a class="zoom-popup" href="assets\imgs\about\about-1-3.jpg"><span class="icon">+</span></a>
                                                </div>
                                            </div>
                                        </div><!-- .text-wrap -->
                                    </div><!-- .gallery-box -->
                                </div>
                            </div><!-- .cbr-carousel-box -->
                        </div>
                    </div><!-- .cbr-content-box -->
                </div><!-- .animate-box -->
            </div><!-- .col-lg-7 -->

            <div class="col-md-12">
                <div class="cbr-spacer clearfix" data-desktop="140" data-mobi="100" data-smobi="100"></div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .row-about-us -->

@include("site.block.processes",["items"=>$processes])

<div class="row-awards has-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                                                
                <div class="cbr-spacer clearfix" data-desktop="69" data-mobi="44" data-smobi="44"></div>

                <div class="cbr-awards style-2">
                    <ul class="awards-list">                                                                                                                                                                   
                    </ul>
                </div><!-- .awards-wrap -->

                <div class="cbr-spacer clearfix" data-desktop="132" data-mobi="93" data-smobi="93"></div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container -->   
</div><!-- .row-awards --> 

@include("site.block.specializations",["items"=>$specializations])





@include("site.block.review_partners")


@include("site.block.team",["items"=>$team])


@include("site.block.articles",["items"=>$articles])


@include("site.block.contact")


                </div><!-- .page-content -->
            </div><!-- #inner-content -->
        </div><!-- #site-content -->
    </div><!-- #content-wrap -->
</div><!-- #main-content -->  

@endsection