<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $fillable = ["item_id","lang","title"];

    public function users(){
        return $this->hasMany(\App\User::class,"item_id","role_id");
    }
}
