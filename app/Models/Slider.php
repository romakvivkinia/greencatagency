<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //
    protected $fillable = ["item_id","lang","title","active","image","link"];


    public function scopeActive($query){
        return $query->where('active',1);
    }
}
