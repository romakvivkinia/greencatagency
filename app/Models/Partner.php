<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    //
    protected $fillable = ["name","active","image","link"];

    public function scopeActive($query){
        return $query->where('active',1);
    }
}
