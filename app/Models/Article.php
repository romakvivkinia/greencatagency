<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //

    protected $fillable = ["category_id","item_id","lang","title","image","active","published_at"];

    public function scopeActive($query){
        return $query->where('active',1);
    }
    
    public function category(){
        return $this->belongsTo(\App\Models\Category::class,"category_id","item_id")
                    ->where(["lang"=>app()->getLocale()]);
    }
}
