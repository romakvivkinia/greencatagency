<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $table = "contacts";
    protected $fillable = ["item_id","lang","facebook","google","image","keywords","title","description","fb","tw","ln","you","phone","tel","email","envelope","address"];
}
