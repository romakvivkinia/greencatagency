<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    //

    protected $fillable = ["item_id","lang","active","title","description","image"];

    public function scopeActive($query){
        return $query->where('active',1);
    }
}
