<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    protected $fillable = ["fullname","active","image","description"];

    public function scopeActive($query){
        return $query->where('active',1);
    }
}
