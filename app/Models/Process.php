<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    //
    protected $fillable = ["item_id","lang","active","title","description"];

    public function scopeActive($query){
        return $query->where('active',1);
    }
}
