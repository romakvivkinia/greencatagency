<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = ["item_id","lang","title"];
    
    public function articles(){
        return $this->hasMany(\App\Models\Article::class,'category_id','item_id');
    }
}
