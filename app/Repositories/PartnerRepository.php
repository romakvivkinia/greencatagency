<?php

namespace App\Repositories;
use App\Models\Partner;
use App\Repositories\RepositoryInterface as Repository;

class PartnerRepository implements Repository {


    protected $model;
    private $data = [];

    // Constructor to bind model to repo
    public function __construct(Partner $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all($paginate = false)
    {
        if ($paginate) {
            return $this->model->paginate($paginate);
        }
        return $this->model->get();
    }

    // create a new record in the database
    public function create($request)
    {
        
       
        return false;
    }

    // update record in the database
    public function update($request, $id)
    {
       
       
        return false;
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }


    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }


    public function find($id){
        return $this->model->findOrFail($id);
    }


    public function getPartners(){
        $this->data = $this->model
                            ->active()
                            
                            //TODO: from config will be better
                            ->take(30)
                            ->get();
        return $this->data;                                
    }

}