<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * Fetch all records by language.
     *
     * @return mixed
     */
    public function all();

    /**
     * Create new record
     *
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * Update or create record.
     *
     * @param $request
     * @param $id
     * @return mixed
     */
    public function update($request, $id);

    /**
     * TODO: Soft delete to be implemented
     *
     * @param $id
     * @return mixed
     */
    public function delete($id);

    public function show($id);

    public function find($id);

    
}