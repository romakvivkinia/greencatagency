<?php

namespace App\Repositories;
use App\User;
use App\Repositories\RepositoryInterface as Repository;

class UserRepository implements Repository {


    protected $model;
    private $data = [];

    // Constructor to bind model to repo
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all($paginate = false)
    {
        if ($paginate) {
            return $this->model->paginate($paginate);
        }
        return $this->model->get();
    }

    // create a new record in the database
    public function create($request)
    {
        $input = $request->except("_token","password");
        $input["email_verified_at"]= \Carbon\Carbon::now()->format("Y-m-d H:i:s");
        $input["avatar"] = $this->uploadAvatar($request->file('avatar'));


        

        if($request->input("password")){
            $input["password"] = \Hash::make($request->input("password"));
        }else{
            $input["password"] = \Hash::make($request->input("123456")); 
        }

        

        if($this->model->fill($input)->save())
            return $this->model;
       
        return false;
    }

    // update record in the database
    public function update($request, $id)
    {
        $record = $this->find($id);
        $input = $request->except("_token","password",'_method');
        

        if($request->input("password")){
            $input["password"] = \Hash::make($request->input("password"));
        }

        if($request->file('avatar')){
            $input["avatar"] = $this->uploadAvatar($request->file('avatar'));
            if($record->avatar){
                $this->deleteFile($record->avatar);
            }
            
        }
        
        // dump($input);
        // dd($request->all());

        if($record->fill($input)->save())
            return $record;
       
        return false;
    }

    // remove record from the database
    public function delete($id)
    {
        $record = $this->find($id);
        if($record->avatar){
            $this->deleteFile($record->avatar);
        }
        
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {

        return $this->model;
    }


    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }


    public function find($id){
        return $this->model->findOrFail($id);
    }


    public function getTeam(){
        return $this->model->where('role_id',4)->take(3)->get();
    }

    public function uploadAvatar($file){
        $path = public_path('files/team/'. date('Y-m-d'));
        if(!\File::exists($path)) {
            \File::makeDirectory($path , $mode = 0777, true, true);
            \File::makeDirectory($path , $mode = 0777, true, true);
            \File::makeDirectory($path , $mode = 0777, true, true);
        }        
        $newName = \Str::random().".jpg";
        $image = \Image::make($file)
            // ->resize(config('statement.image_size.high.width'),config('statement.image_size.high.height'))
            //TODO: beter from config
            ->fit(370,370);
            
        // wathermark
        // $image->insert(public_path('assets/site/img/logo.png'),'bottom-left',10,10);

        $image->save("{$path}/{$newName}");
        $imagePath = 'files/team/'. date('Y-m-d')."/{$newName}";
                
       return $imagePath;     
            
    }
    public function deleteFile($oath){
        try {
            unlink(public_path($oath));
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }
    }




    public function getAllItems(){
        $this->data["data"]   = $this->model
                                            
                                            ->with(["role"])    
                                            ->select(
                                                "users.id",
                                                "users.role_id",
                                                "users.first_name",
                                                "users.last_name",
                                                "users.email",
                                                "users.avatar",
                                                "users.id as cogs"
                                            );




            //Search Start

            //default search
            if(!empty(request()->input('search.value'))){
                $searchWord = trim(request()->input('search.value'));
                if(is_numeric($searchWord)){
                    $this->data["data"] = $this->data["data"]->where(function($query)use($searchWord){
                       $query->where('users.id',$searchWord);
                       
                    });
                }else{
                    $this->data["data"] = $this->data["data"]->where(function($query)use($searchWord){
                        $query->where('users.title','LIKE',$searchWord."%");
                        
                    });
                }
            }









            /*
            * Ordering by column
            * */

            if(request()->input("order")){
                //Get ordered column and value
                $this->data["list"] = ["id","first_name","last_name","avatar"];
                $this->data["order"] = collect(request()->input("order")[0]);
                $col  = (int)$this->data["order"]["column"];
                $dir  = strtoupper($this->data["order"]["dir"]);
                $this->data["column"] = !empty($this->data["list"][$col]) ? $this->data["list"][$col]: "";
                $this->data["value"]  = $dir;
                if(!empty($this->data["column"]) && !empty($this->data["value"])){
                    $this->data["data"] = $this->data["data"]->orderBy($this->data["column"],$this->data["value"]);
                }
            }


            $this->data["draw"]   = request()->input("draw");


            $this->data["recordsFiltered"] =  $this->data["data"]->count();
            

            $this->data["length"] = (int)request()->input('length');
            $this->data["length"] = $this->data["length"]==-1 ? $this->data["data"]->count() : $this->data["length"] ;


            $this->data["data"]           = $this->data["data"]
                                                ->skip((int)request()->input('start'))
                                                ->take($this->data["length"])
                                                ->get();
          

            return $this->data;    
    }


}