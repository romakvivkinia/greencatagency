<?php

namespace App\Repositories;
use App\Models\Specialization;
use App\Repositories\RepositoryInterface as Repository;

class SpecializationRepository implements Repository {


    protected $model;
    private $data = [];

    // Constructor to bind model to repo
    public function __construct(Specialization $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all($paginate = false)
    {
        if ($paginate) {
            return $this->model->paginate($paginate);
        }
        return $this->model->get();
    }

    // create a new record in the database
    public function create($request)
    {
        $input = $request->except("_token","_method");
        $input["lang"] = app()->getLocale();

        if( $this->model->fill($input)->save()){
            $this->model->item_id = $this->model->id;
            $this->model->save();
            return $this->model;
        }
            
        return false;
    }

    // update record in the database
    public function update($request, $id)
    {
        $record = $this->find($id);
        $input = $request->except("_token");
        $input["lang"] = app()->getLocale();
        $input["active"] = $request->input('active') ? 1 : 0;
        if($record->lang == app()->getLocale())
            return $record->fill($input)->save();
        else{
            $input["item_id"] = $record->item_id;
            return $this->model->fill($input)->save();
        }    
       
        return false;
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }


    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }


    public function find($id){
        return $this->model->findOrFail($id);
    }


    public function getSpecializations(){
        $this->data = $this->model
                            ->active()
                            ->where([
                                "lang"=>app()->getLocale()
                            ])
                            //TODO: from config will be better
                            ->take(10)
                            ->get();
        return $this->data;                                
    }


    public function getAllItems(){
        $this->data["data"]   = $this->model
                                            ->where(["lang"=>app()->getLocale()])
                                            ->select(
                                                "specializations.id",
                                                "specializations.item_id",
                                                "specializations.active",
                                                "specializations.title",
                                                "specializations.description",
                                                "specializations.image",
                                                "specializations.id as cogs"
                                            );

            //Search Start

            //default search
            if(!empty(request()->input('search.value'))){
                $searchWord = trim(request()->input('search.value'));
                if(is_numeric($searchWord)){
                    $this->data["data"] = $this->data["data"]->where(function($query)use($searchWord){
                       $query->where('specializations.id',$searchWord);
                       
                    });
                }else{
                    $this->data["data"] = $this->data["data"]->where(function($query)use($searchWord){
                        $query->where('specializations.title','LIKE',$searchWord."%");
                        
                    });
                }
            }


            /*
            * Ordering by column
            * */

            if(request()->input("order")){
                //Get ordered column and value
                $this->data["list"] = ["id","active","title","description","image"];
                $this->data["order"] = collect(request()->input("order")[0]);
                $col  = (int)$this->data["order"]["column"];
                $dir  = strtoupper($this->data["order"]["dir"]);
                $this->data["column"] = !empty($this->data["list"][$col]) ? $this->data["list"][$col]: "";
                $this->data["value"]  = $dir;
                if(!empty($this->data["column"]) && !empty($this->data["value"])){
                    $this->data["data"] = $this->data["data"]->orderBy($this->data["column"],$this->data["value"]);
                }
            }


            $this->data["draw"]   = request()->input("draw");


            $this->data["recordsFiltered"] =  $this->data["data"]->count();
            

            $this->data["length"] = (int)request()->input('length');
            $this->data["length"] = $this->data["length"]==-1 ? $this->data["data"]->count() : $this->data["length"] ;


            $this->data["data"]           = $this->data["data"]
                                                ->skip((int)request()->input('start'))
                                                ->take($this->data["length"])
                                                ->get();
          

            return $this->data;    
    }

}