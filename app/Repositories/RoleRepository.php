<?php

namespace App\Repositories;
use App\Models\Role;
use App\Repositories\RepositoryInterface as Repository;

class RoleRepository implements Repository {


    protected $model;
    private $data = [];

    // Constructor to bind model to repo
    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all($paginate = false)
    {
        if ($paginate) {
            return $this->model->where(["lang"=>app()->getLocale()])->paginate($paginate);
        }
        return $this->model->where(["lang"=>app()->getLocale()])->get();
    }

    // create a new record in the database
    public function create($request)
    {
       
        return false;
    }

    // update record in the database
    public function update($request, $id)
    {
       
        return false;
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }


    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }


    public function find($id){
        $record = $this->model->where(["lang"=>app()->getLocale(),"item_id"=>$id])->first();
        
        if($record)
            return $record;

        return  $this->model->where(["item_id"=>$id])->first();
    }

}