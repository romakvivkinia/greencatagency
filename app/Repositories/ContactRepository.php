<?php

namespace  App\Repositories;
use App\Models\Contact;
class ContactRepository
{

    public function __construct(Contact $s_rep)
    {
        $this->model = $s_rep;
    }
    public  function find($id)
    {
        $record = $this->model->where(['item_id'=>$id,'lang'=>app()->getLocale()])->first();

        if($record)
            return $record;
       return  $this->model->where(['item_id'=>$id])->first();
    }


    public function first($id)
    {
        
        return $this->model->findOrFail((int)$id);
    }
    public function update($request, $record)
    {
        $response = false;
        $record   = $this->find((int)$record);
        $input =$request->except("_method","_token");
        // dd($input);
       
        if($record->lang==app()->getLocale()){
            $record->fill($input);
            if($record->save())
                $response =__("dashboard.dashboard_item_created");
            else
                $response = false;

        }else{
           
            $input["lang"] = app()->getLocale();
            $input=$this->model->fill($input);
            if($input->save()){
                $input->item_id = $record->item_id;
                $response = __("dashboard.dashboard_item_updated");
                $input->save();
            }

        }
        return $response;

    }
}

