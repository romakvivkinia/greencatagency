<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  auth()->check() && auth()->user()->role_id==1;;
    }

    protected function getValidatorInstance(){
        $validator = parent::getValidatorInstance();


       $validator->sometimes("password","required|string|min:6|max:191",function($input){
           return !empty($input->password);
       });

       $validator->sometimes("avatar","mimes:jpeg,jpg,png|required",function($input){
        return !empty($input->avatar);
       });


        return $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
                "email"    => "required|string|max:191|email|unique:users,email" . (request()->input('item') ? ",".request()->input('item') : '' ),
                "first_name" => "required|string|max:191",
                "last_name" => "required|string|max:191",
                "role_id"  => "required|exists:roles,item_id"
            
        ];
    }
}
