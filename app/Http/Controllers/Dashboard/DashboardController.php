<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    protected $data = [];

    protected $template=null;

    protected function render(){
        return view($this->template,$this->data);
    }
}
