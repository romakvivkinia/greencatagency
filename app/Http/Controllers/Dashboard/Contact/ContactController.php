<?php

namespace App\Http\Controllers\Dashboard\Contact;


use Illuminate\Http\Request;

use App\Http\Controllers\Dashboard\DashboardController as Controller;

use App\Repositories\ContactRepository;

class ContactController extends Controller
{
    public function __construct(ContactRepository $c_rep){
        $this->c_rep = $c_rep;
        $this->template = "dashboard.page.contact.";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data["contact"] = $this->c_rep->find($id);
        // dd($this->data);
        $this->template .= "edit";
        
        return $this->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $result  = $this->c_rep->update($request,$id);



        if($result){
            return redirect()->back()->with(["success"=>__("dashboard.dashboard_item_updateded")]);
        }
        return redirect()->back()->withErrors(["errors"=>"Error"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
