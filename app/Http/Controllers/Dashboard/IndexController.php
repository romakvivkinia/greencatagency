<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

class IndexController extends DashboardController
{
    //
    public function __construct(){
        $this->template = "dashboard.page.";
        
    }

    public function index(){

        $this->template .= "index";
        return $this->render();
    }
}
