<?php

namespace App\Http\Controllers\Dashboard\Slider;

use App\Http\Controllers\Dashboard\DashboardController as Controller;
use Illuminate\Http\Request;
use App\Repositories\SliderRepository;
use App\Http\Requests\SliderRequest;

class SliderController extends Controller
{
    public function __construct(SliderRepository $slider_rep){
        $this->slider_rep = $slider_rep;
        $this->template = "dashboard.page.slider.";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(request()->ajax()){
            $this->data = $this->slider_rep->getAllItems();
            return response()->json($this->data);
        }
        $this->template .= "index";
        return $this->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->template .= "create";
        return $this->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        //
        $this->data["response"] = $this->slider_rep->create($request);
        if($this->data["response"])
            return redirect()->route("slider.edit",[$this->data["response"]->item_id])->with(["success"=>__("dashboard.dashboard_item_saved")]);
        
        return redirect()->back()->withErrors(["errors"=>"Error"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data["item"] = $this->slider_rep->find($id);
        // dd($this->data);
        $this->template .= "edit";
        return $this->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->data["response"] = $this->slider_rep->update($request,$id);
        if($this->data["response"])
            return redirect()->back()->with(["success"=>__("dashboard.dashboard_item_updateded")]);
        
        return redirect()->back()->withErrors(["errors"=>"Error"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->slider_rep->delete($id))
            return response()->json(["success"=>true]);
        return response()->json(["success"=>false],500);
    }
}
