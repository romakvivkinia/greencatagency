<?php

namespace App\Http\Controllers\Dashboard\User;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Dashboard\DashboardController as Controller;

class UserController extends Controller
{
    public function __construct(RoleRepository $role_rep,UserRepository $user_rep){
        $this->user_rep = $user_rep;
        $this->role_rep = $role_rep;
        $this->template = "dashboard.page.user.";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(request()->ajax()){
            $this->data = $this->user_rep->getAllItems();
            return response()->json($this->data);
        }
        $this->template .= "index";
        return $this->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->data["roles"] = $this->role_rep->all();
        $this->template .= "create";
        return $this->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        //
        $this->data["response"] = $this->user_rep->create($request);
        if($this->data["response"])
            return redirect()->route("user.edit",[$this->data["response"]->id])->with(["success"=>__("dashboard.dashboard_item_saved")]);
        
        return redirect()->back()->withErrors(["errors"=>"Error"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data["roles"] = $this->role_rep->all();
        $this->data["item"] = $this->user_rep->find($id);
        $this->template .= "edit";
        return $this->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->data["response"] = $this->user_rep->update($request,$id);
        if($this->data["response"])
            return redirect()->back()->with(["success"=>__("dashboard.dashboard_item_updateded")]);
        
        return redirect()->back()->withErrors(["errors"=>"Error"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->user_rep->delete($id))
            return response()->json(["success"=>true]);
        return response()->json(["success"=>false],500);
    }
}
