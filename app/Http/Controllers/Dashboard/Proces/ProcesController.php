<?php

namespace App\Http\Controllers\Dashboard\Proces;

use App\Http\Controllers\Dashboard\DashboardController as Controller;
use Illuminate\Http\Request;
use App\Repositories\ProcessRepository;
use App\Http\Requests\ProcesRequest;

class ProcesController extends Controller
{
    public function __construct(ProcessRepository $proces_rep){
        $this->proces_rep = $proces_rep;
        $this->template = "dashboard.page.process.";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(request()->ajax()){
            $this->data = $this->proces_rep->getAllItems();
            return response()->json($this->data);
        }
        $this->template .= "index";
        return $this->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->template .= "create";
        return $this->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProcesRequest $request)
    {
        //
        $this->data["response"] = $this->proces_rep->create($request);
        if($this->data["response"])
            return redirect()->route("process.edit",[$this->data["response"]->item_id])->with(["success"=>__("dashboard.dashboard_item_saved")]);
        
        return redirect()->back()->withErrors(["errors"=>"Error"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data["item"] = $this->proces_rep->find($id);
        $this->template .= "edit";
        return $this->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProcesRequest $request, $id)
    {
        //
        $this->data["response"] = $this->proces_rep->update($request,$id);
        if($this->data["response"])
            return redirect()->back()->with(["success"=>__("dashboard.dashboard_item_updateded")]);
        
        return redirect()->back()->withErrors(["errors"=>"Error"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->proces_rep->delete($id))
            return response()->json(["success"=>true]);
        return response()->json(["success"=>false],500);
    }
}
