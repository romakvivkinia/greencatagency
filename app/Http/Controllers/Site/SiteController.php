<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    //
    protected $data = [];

    

    protected $template=null;
    
    protected $sideBar=false;

    protected function render(){
        $this->data["sideBar"] = $this->sideBar;
        return view($this->template,$this->data);
    }
}
