<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\SpecializationRepository;
use App\Repositories\PartnerRepository;

class AboutController extends SiteController
{
    //
    public function __construct(PartnerRepository $partner_rep,SpecializationRepository $spec_rep,UserRepository $user_rep,ReviewRepository $review_rep){
        $this->template = "site.page.about.";
        $this->user_rep = $user_rep;
        $this->review_rep = $review_rep;
        $this->spec_rep = $spec_rep;
        $this->partner_rep = $partner_rep;
    }

    public function index(){
        
        $this->data["partners"] = $this->partner_rep->getPartners();
        $this->data["specializations"] = $this->spec_rep->getSpecializations();
        $this->data["team"] = $this->user_rep->getTeam();
        $this->data["reviews"] = $this->review_rep->getReviews();
        $this->template .= "index";
        return $this->render();
    }
}
