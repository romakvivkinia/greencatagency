<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContactUsRequest;

class ContactController extends SiteController
{
    //

    public function send(ContactUsRequest $request){

        
        //TODO: need real mail sever
        // \Mail::send('site.block.send_email',["data"=>$request->all()], function($message){
        //     $message->from('info@company.io');
        //     $message->to(app('meta')->email)
        //     ->subject('company');
        // });

        return response()->json(["success"=>true,"message"=>__("dashboard.dashboard_item_saved")]);
    }

    
}
