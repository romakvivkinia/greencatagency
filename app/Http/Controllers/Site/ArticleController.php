<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ArticleRepository;
class ArticleController extends SiteController
{
    //

    public function __construct(ArticleRepository $article_rep){
        $this->template = "site.page.articles.";
        $this->sideBar = true;
        $this->article_rep = $article_rep;
    }

    public function index(){
        $this->data["items"] = $this->article_rep->getArticles();
        $this->template .= "index";
        return $this->render();
    }
}
