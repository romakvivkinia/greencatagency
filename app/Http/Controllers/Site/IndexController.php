<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SliderRepository;
use App\Repositories\ProcessRepository;
use App\Repositories\SpecializationRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\PartnerRepository;
use App\Repositories\UserRepository;
use App\Repositories\ArticleRepository;

class IndexController extends SiteController
{
    //
    public function __construct(
        SliderRepository $slider_rep,
        ProcessRepository $process_rep,
        SpecializationRepository $spec_rep,
        ReviewRepository $review_rep,
        PartnerRepository $partner_rep,
        UserRepository $user_rep,
        ArticleRepository $article_rep
    ){
        $this->template = "site.page.";
        $this->slider_rep = $slider_rep;
        $this->process_rep = $process_rep;
        $this->spec_rep = $spec_rep;
        $this->review_rep = $review_rep;
        $this->partner_rep = $partner_rep;
        $this->user_rep = $user_rep;
        $this->article_rep = $article_rep;
        
    }

    public function index(){

        $this->data["sliders"] = $this->slider_rep->getSliders();
        $this->data["processes"] = $this->process_rep->getProcesses();
        $this->data["specializations"] = $this->spec_rep->getSpecializations();
        $this->data["partners"] = $this->partner_rep->getPartners();
        $this->data["reviews"] = $this->review_rep->getReviews();
        $this->data["team"] = $this->user_rep->getTeam();
        $this->data["articles"] = $this->article_rep->getLatesArticles();

        // dd($this->data);

        $this->template .= "index";
        return $this->render();
    }
}
