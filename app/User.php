<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'first_name','last_name','email', 'password','bio','ln','fb','gm','tw','avatar','possition'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * for filter memeber of company
     */
    public function scopeTeamMembers($query){
        return $query->whereIn('role_id',[2]);
    }

    /**
     * translated user role
     */
    public function role(){
        return $this->belongsTo(\App\Models\Role::class,"role_id","item_id")->where(["roles.lang"=>app()->getLocale()]);
    }
}
