<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        Schema::defaultStringLength(191);
        // Resource::withoutWrapping();


        /**
         * SEO
         */

        $this->app->singleton("meta_og",function($app){
            
            return \App\Models\Contact::where(["item_id"=>1,"lang"=>app()->getLocale()])->first();
        });

        $this->app->singleton("meta",function($app){
            return \App\Models\Contact::where(["item_id"=>4,"lang"=>app()->getLocale()])->first();
        });
    }
}
