<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/








Auth::routes();
Route::group(['prefix' => LaravelLocalization::setLocale()], function(){
	Route::get("/",["uses"=>"Site\IndexController@index","as"=>"home"]);
	Route::get("/articles",["uses"=>"Site\ArticleController@index","as"=>"articles"]);
	Route::get("/about",["uses"=>"Site\AboutController@index","as"=>"about"]);
	Route::post("/send",["uses"=>"Site\ContactController@send","as"=>"send_contact"]);


    	//dashboard
	Route::group(['prefix' => "dashboard","namespace"=>"Dashboard","middleware"=>["auth"]],function(){
		Route::get("/",["uses"=>"IndexController@index","as"=>"dashboard"]);
		Route::resource("/contact","Contact\ContactController");
		Route::resource("/slider","Slider\SliderController");
		Route::resource("/process","Proces\ProcesController");
		Route::resource("/specialization","Specialization\SpecializationController");
		Route::resource("/user","User\UserController");

		


		//file manager
		Route::get('glide/{path}', function($path){
			$server = \League\Glide\ServerFactory::create([
				'source' => app('filesystem')->disk('public')->getDriver(),
				'cache' => storage_path('glide'),
			]);
			return $server->getImageResponse($path, Input::query());
        })->where('path', '.+');
        
	});

	

});