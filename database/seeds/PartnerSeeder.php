<?php

use Illuminate\Database\Seeder;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $items = [
            [
                "image"=>"files/partners/2020-07-18/1.png"
            ],

            [
                "image"=>"files/partners/2020-07-18/2.png"
            ],
            [
                "image"=>"files/partners/2020-07-18/3.png"
            ],

        ];

        foreach($items as $key=>$item){
            \App\Models\Partner::create($item);
        }
    }
}
