<?php

use Illuminate\Database\Seeder;

class SpecializationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $itemsKA  = [
            ["title"=>"სპეციალიზაცია 3" , "lang"=>"ka","description"=>"ჩვენი სპეციალიზაცია - დუბლირდება გვერდიდან „ჩვენ შესახებ“.","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
            ["title"=>"სპეციალიზაცია 1" , "lang"=>"ka","description"=>"ჩვენი სპეციალიზაცია - დუბლირდება გვერდიდან „ჩვენ შესახებ“.","image"=>"files/projects/2020-07-18/project1-370x520.jpg"],
            ["title"=>"სპეციალიზაცია 4" , "lang"=>"ka","description"=>"ჩვენი სპეციალიზაცია - დუბლირდება გვერდიდან „ჩვენ შესახებ“.","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
            ["title"=>"სპეციალიზაცია 2" , "lang"=>"ka","description"=>"ჩვენი სპეციალიზაცია - დუბლირდება გვერდიდან „ჩვენ შესახებ“.","image"=>"files/projects/2020-07-18/project2-370x520.jpg"],
            ["title"=>"სპეციალიზაცია 5" , "lang"=>"ka","description"=>"ჩვენი სპეციალიზაცია - დუბლირდება გვერდიდან „ჩვენ შესახებ“.","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
        ];

        $itemsEN  = [
            ["title"=>"Specialization 3", "lang"=>"en","description"=>"Our specialization - duplicated from the page \"About us\".","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
            ["title"=>"Specialization 1", "lang"=>"en","description"=>"Our specialization - duplicated from the page \"About us\".","image"=>"files/projects/2020-07-18/project1-370x520.jpg"],
            ["title"=>"Specialization 4", "lang"=>"en","description"=>"Our specialization - duplicated from the page \"About us\".","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
            ["title"=>"Specialization 2", "lang"=>"en","description"=>"Our specialization - duplicated from the page \"About us\".","image"=>"files/projects/2020-07-18/project2-370x520.jpg"],
            ["title"=>"Specialization 5", "lang"=>"en","description"=>"Our specialization - duplicated from the page \"About us\".","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
        ];

        $itemsRU  = [
            ["title"=>"специализация 3", "lang"=>"ru","description"=>"Наша специализация - продублировано со страницы «О нас».","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
            ["title"=>"специализация 1", "lang"=>"ru","description"=>"Наша специализация - продублировано со страницы «О нас».","image"=>"files/projects/2020-07-18/project1-370x520.jpg"],
            ["title"=>"специализация 4", "lang"=>"ru","description"=>"Наша специализация - продублировано со страницы «О нас».","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
            ["title"=>"специализация 2", "lang"=>"ru","description"=>"Наша специализация - продублировано со страницы «О нас».","image"=>"files/projects/2020-07-18/project2-370x520.jpg"],
            ["title"=>"специализация 5", "lang"=>"ru","description"=>"Наша специализация - продублировано со страницы «О нас».","image"=>"files/projects/2020-07-18/project3-370x520.jpg"],
        ];


        foreach($itemsKA as $key=>$role){
            $newItem = \App\Models\Specialization::create($role);

            $newItem->item_id = $newItem->id;
            $newItem->save();

            if($newItem){
                $newItemEN = \App\Models\Specialization::create($itemsEN[$key]);
                $newItemEN->item_id = $newItem->id;
                $newItemEN->save();


                $newItemRU = \App\Models\Specialization::create($itemsRU[$key]);
                $newItemRU->item_id = $newItem->id;
                $newItemRU->save();
            }
        }
    }
}
