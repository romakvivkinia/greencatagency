<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $itemsKA  = [
            ["title"=>"სპორტი" , "lang"=>"ka"],
        ];

        $itemsEN  = [
            ["title"=>"Sport", "lang"=>"en"],
        ];

        $itemsRU  = [
            ["title"=>"спорт", "lang"=>"ru"],
        ];


        foreach($itemsKA as $key=>$item){
            $newItem = \App\Models\Category::create($item);

            $newItem->item_id = $newItem->id;
            $newItem->save();

            if($newItem){
                $newItemEN = \App\Models\Category::create($itemsEN[$key]);
                $newItemEN->item_id = $newItem->id;
                $newItemEN->save();


                $newItemRU = \App\Models\Category::create($itemsRU[$key]);
                $newItemRU->item_id = $newItem->id;
                $newItemRU->save();
            }
        }
    
    }
}
