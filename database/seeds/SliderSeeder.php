<?php

use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $itemsKA  = [
            ["title"=>"სლიდერ 1" , "lang"=>"ka","image"=>"files/slider/2020-07-18/slider2.jpg","link"=>asset('files/slider/2020-07-18/slider2.jpg')],
            ["title"=>"სლიდერ 2" , "lang"=>"ka","image"=>"files/slider/2020-07-18/slider3.jpg","link"=>asset('files/slider/2020-07-18/slider3.jpg')],
            ["title"=>"სლიდერ 3" , "lang"=>"ka","image"=>"files/slider/2020-07-18/slider4.jpg","link"=>asset('files/slider/2020-07-18/slider4.jpg')],
        ];

        $itemsEN  = [
            ["title"=>"Slider 1", "lang"=>"en","image"=>"files/slider/2020-07-18/slider2.jpg","link"=>asset('files/slider/2020-07-18/slider2.jpg')],
            ["title"=>"Slider 2", "lang"=>"en","image"=>"files/slider/2020-07-18/slider3.jpg","link"=>asset('files/slider/2020-07-18/slider3.jpg')],
            ["title"=>"Slider 3", "lang"=>"en","image"=>"files/slider/2020-07-18/slider4.jpg","link"=>asset('files/slider/2020-07-18/slider4.jpg')]
        ];

        $itemsRU  = [
            ["title"=>"Ползунок 1", "lang"=>"ru","image"=>"files/slider/2020-07-18/slider2.jpg","link"=>asset('files/slider/2020-07-18/slider2.jpg')],
            ["title"=>"Ползунок 2", "lang"=>"ru","image"=>"files/slider/2020-07-18/slider3.jpg","link"=>asset('files/slider/2020-07-18/slider3.jpg')],
            ["title"=>"Ползунок 3", "lang"=>"ru","image"=>"files/slider/2020-07-18/slider4.jpg","link"=>asset('files/slider/2020-07-18/slider4.jpg')]
        ];


        foreach($itemsKA as $key=>$role){
            $newItem = \App\Models\Slider::create($role);

            $newItem->item_id = $newItem->id;
            $newItem->save();

            if($newItem){
                $newItemEN = \App\Models\Slider::create($itemsEN[$key]);
                $newItemEN->item_id = $newItem->id;
                $newItemEN->save();


                $newItemRU = \App\Models\Slider::create($itemsRU[$key]);
                $newItemRU->item_id = $newItem->id;
                $newItemRU->save();
            }
        }
    }
}
