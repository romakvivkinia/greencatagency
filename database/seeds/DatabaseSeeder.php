<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(ProcessesSeeder::class);
        $this->call(SpecializationSeeder::class);
        $this->call(ReviewSeeder::class);
        $this->call(PartnerSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(ContactSeeder::class);
    }
}
