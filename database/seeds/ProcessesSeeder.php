<?php

use Illuminate\Database\Seeder;

class ProcessesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $itemsKA  = [
            ["title"=>"პროცესი 1" , "lang"=>"ka","description"=>"სათაური და ტექსტი (შეზღუდული სიმბოლოების რაოდენობით)"],
            ["title"=>"პროცესი 2" , "lang"=>"ka","description"=>"სათაური და ტექსტი (შეზღუდული სიმბოლოების რაოდენობით)"],
            ["title"=>"პროცესი 3" , "lang"=>"ka","description"=>"სათაური და ტექსტი (შეზღუდული სიმბოლოების რაოდენობით)"],
        ];

        $itemsEN  = [
            ["title"=>"Process 1", "lang"=>"en","description"=>"Title and text (limited number of characters)"],
            ["title"=>"Process 2", "lang"=>"en","description"=>"Title and text (limited number of characters)"],
            ["title"=>"Process 3", "lang"=>"en","description"=>"Title and text (limited number of characters)"]
        ];

        $itemsRU  = [
            ["title"=>"Ползунок 1", "lang"=>"ru","description"=>"Заголовок и текст (ограниченное количество символов)"],
            ["title"=>"Ползунок 2", "lang"=>"ru","description"=>"Заголовок и текст (ограниченное количество символов)"],
            ["title"=>"Ползунок 3", "lang"=>"ru","description"=>"Заголовок и текст (ограниченное количество символов)"]
        ];


        foreach($itemsKA as $key=>$item){
            $newItem = \App\Models\Process::create($item);

            $newItem->item_id = $newItem->id;
            $newItem->save();

            if($newItem){
                $newItemEN = \App\Models\Process::create($itemsEN[$key]);
                $newItemEN->item_id = $newItem->id;
                $newItemEN->save();


                $newItemRU = \App\Models\Process::create($itemsRU[$key]);
                $newItemRU->item_id = $newItem->id;
                $newItemRU->save();
            }
        }
    }
}
