<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       

        $itemsKA  = [
            ["lang"=>"ka","facebook"=>'fbid',"google"=>"gaid","image"=>"","title"=>"Site Page title","keywords"=>"","description"=>"keywords","address"=>"თბილისი, საქართველო"],
            ["lang"=>"ka","image"=>'',"fb"=>"","tw"=>"","ln"=>"","you"=>"","address"=>"addres ka","phone"=>"+955 568 52-55-56","tel"=>"+ 995 322 10 01 10","email"=>"info@company.ge","envelope"=>"contact@company.ge"]
        ];

        $itemsEN  = [
            ["lang"=>"en","facebook"=>'fbid',"google"=>"gaid","image"=>"","title"=>"Site Page title","keywords"=>"","description"=>"keywords","address"=>"თბილისი, საქართველო"],
            ["lang"=>"en","image"=>'',"fb"=>"","tw"=>"","ln"=>"","you"=>"","address"=>"addres ka","phone"=>"+955 568 52-55-56","tel"=>"+ 995 322 10 01 10","email"=>"info@company.ge","envelope"=>"contact@company.ge"]
        ];

        $itemsRU  = [
            ["lang"=>"ru","facebook"=>'fbid',"google"=>"gaid","image"=>"","title"=>"Site Page title","keywords"=>"","description"=>"keywords","address"=>"თბილისი, საქართველო"],
            ["lang"=>"ru","image"=>'',"fb"=>"","tw"=>"","ln"=>"","you"=>"","address"=>"addres ka","phone"=>"+955 568 52-55-56","tel"=>"+ 995 322 10 01 10","email"=>"info@company.ge","envelope"=>"contact@company.ge"]
        ];

       


        foreach($itemsKA as $key=>$item){
            $newItem = \App\Models\Contact::create($item);

            $newItem->item_id = $newItem->id;
            $newItem->save();

            if($newItem){
                $newItemEN = \App\Models\Contact::create($itemsEN[$key]);
                $newItemEN->item_id = $newItem->id;
                $newItemEN->save();


                $newItemRU = \App\Models\Contact::create($itemsRU[$key]);
                $newItemRU->item_id = $newItem->id;
                $newItemRU->save();
            }
        }
    }
}
