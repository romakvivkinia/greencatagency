<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        $category = \App\Models\Category::where(["lang"=>app()->getLocale()])->first();

        $itemsKA  = [
            ["category_id"=>$category->id,"title"=>"სპორტი 1" , "lang"=>"ka","image"=>"files/articles/2020-07-18/blog-grid1-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
            ["category_id"=>$category->id,"title"=>"სპორტი 2" , "lang"=>"ka","image"=>"files/articles/2020-07-18/blog-grid2-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
            ["category_id"=>$category->id,"title"=>"სპორტი 3" , "lang"=>"ka","image"=>"files/articles/2020-07-18/blog-grid3-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
        ];

        $itemsEN  = [
            ["category_id"=>$category->id,"title"=>"Sport 1", "lang"=>"en","image"=>"files/articles/2020-07-18/blog-grid1-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
            ["category_id"=>$category->id,"title"=>"Sport 2", "lang"=>"en","image"=>"files/articles/2020-07-18/blog-grid2-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
            ["category_id"=>$category->id,"title"=>"Sport 3", "lang"=>"en","image"=>"files/articles/2020-07-18/blog-grid3-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
        ];

        $itemsRU  = [
            ["category_id"=>$category->id,"title"=>"спорт 1", "lang"=>"ru","image"=>"files/articles/2020-07-18/blog-grid1-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
            ["category_id"=>$category->id,"title"=>"спорт 2", "lang"=>"ru","image"=>"files/articles/2020-07-18/blog-grid2-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
            ["category_id"=>$category->id,"title"=>"спорт 3", "lang"=>"ru","image"=>"files/articles/2020-07-18/blog-grid3-370x275.jpg","published_at"=>\Carbon\Carbon::now()],
        ];


        foreach($itemsKA as $key=>$item){
            $newItem = \App\Models\Article::create($item);

            $newItem->item_id = $newItem->id;
            $newItem->save();

            if($newItem){
                $newItemEN = \App\Models\Article::create($itemsEN[$key]);
                $newItemEN->item_id = $newItem->id;
                $newItemEN->save();


                $newItemRU = \App\Models\Article::create($itemsRU[$key]);
                $newItemRU->item_id = $newItem->id;
                $newItemRU->save();
            }
        }
    }
}
