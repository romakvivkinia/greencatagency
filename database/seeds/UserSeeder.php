<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data = [
            [
                "role_id" => 1,
                "first_name"=>"Roma",
                "last_name" =>"Roma",
                "email" => "romaxob@mail.ru",
                "password"=>\Hash::make("rocketman"),
                "email_verified_at" =>\Carbon\Carbon::now()->format("Y-m-d H:i:s")
            ],
            [
                "role_id" => 1,
                "first_name"=>"Admin",
                "last_name" =>"admin",
                "email" => "admin@admin.ge",
                "password"=>\Hash::make("admin123"),
                "email_verified_at" =>\Carbon\Carbon::now()->format("Y-m-d H:i:s")
            ],
            [
                "role_id" => 4,
                "first_name"=>"Name2",
                "last_name" =>"Last Name2",
                "possition" => "Engineer",
                "bio" =>"We use design to enrich people’s lives and help organizations succeed. Our 1,700 people collaborate across a network of 23 offices on three continents.",
                "avatar" => "files/team/2020-07-18/person4-370x370.jpg",
                "email" => "member2@admin.ge",
                "password"=>\Hash::make("123asdASD"),
                "email_verified_at" =>\Carbon\Carbon::now()->format("Y-m-d H:i:s")
            ],
            [
                "role_id" => 4,
                "first_name"=>"John",
                "last_name" =>"Doe",
                "possition" => "Hacker",
                "bio" =>"We use design to enrich people’s lives and help organizations succeed. Our 1,700 people collaborate across a network of 23 offices on three continents.",
                "avatar" => "files/team/2020-07-18/person1-370x370.jpg",
                "email" => "member1@admin.ge",
                "password"=>\Hash::make("123asdASD"),
                "email_verified_at" =>\Carbon\Carbon::now()->format("Y-m-d H:i:s")
            ],
            
            [
                "role_id" => 4,
                "first_name"=>"Name3",
                "last_name" =>"Last Name3",
                "possition" => "SEO",
                "bio" =>"We use design to enrich people’s lives and help organizations succeed. Our 1,700 people collaborate across a network of 23 offices on three continents.",
                "avatar" => "files/team/2020-07-18/person4-370x370.jpg",
                "email" => "member3@admin.ge",
                "password"=>\Hash::make("123asdASD"),
                "email_verified_at" =>\Carbon\Carbon::now()->format("Y-m-d H:i:s")
            ],
        ];

        foreach($data as $user){
            \App\User::create($user);
        }

    }
}
