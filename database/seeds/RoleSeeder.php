<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $rolesKA  = [
            ["title"=>"ადმინი" , "lang"=>"ka"],
            ["title"=>"თანამშრომელი" , "lang"=>"ka"],
            ["title"=>"მომხმარებელი" , "lang"=>"ka"],
        ];

        $rolesEN  = [
            ["title"=>"Admin", "lang"=>"en"],
            ["title"=>"Employee", "lang"=>"en"],
            ["title"=>"User", "lang"=>"en"]
        ];

        $rolesRU  = [
            ["title"=>"Админ", "lang"=>"ru"],
            ["title"=>"Работник", "lang"=>"ru"],
            ["title"=>"Пользователь", "lang"=>"ru"]
        ];


        foreach($rolesKA as $key=>$role){
            $newRole = \App\Models\Role::create($role);

            $newRole->item_id = $newRole->id;
            $newRole->save();

            if($newRole){
                $newRoleEN = \App\Models\Role::create($rolesEN[$key]);
                $newRoleEN->item_id = $newRole->id;
                $newRoleEN->save();


                $newRoleRU = \App\Models\Role::create($rolesRU[$key]);
                $newRoleRU->item_id = $newRole->id;
                $newRoleRU->save();
            }
        }
    
    }
}
