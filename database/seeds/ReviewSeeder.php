<?php

use Illuminate\Database\Seeder;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        $items =  [
            [
                "fullname"   => "Jacqueline Guyon",
                "image"      => "files/reviews/2020-07-18/1.png",
                "description" => "For people. Preferably for you – and for all the things you find important in life. Every project we undertake is just as uniqueiinterpretation of your character."
            ],
            [
                "fullname"   => "Jacqueline Guyon",
                "image"      => "files/reviews/2020-07-18/2.png",
                "description" => "For people. Preferably for you – and for all the things you find important in life. Every project we undertake is just as uniqueiinterpretation of your character."
            ],
            [
                "fullname"   => "Faye Hamilton",
                "image"      => "files/reviews/2020-07-18/3.png",
                "description" => "We are looking forward to our project standing shoulder to shoulder with the others on your website, although we do think our flat is the best!"
            ]
        ];

        foreach($items as $key=>$item){
            \App\Models\Review::create($item);
        }
    }
}
