<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->integer("item_id")->unsigned()->nullable()->default(null);
            $table->string("lang",2)->default(app()->getLocale());
            $table->string("facebook")->nullable()->default(Null);
            $table->string("google")->nullable()->default(Null);
            $table->string("image")->nullable()->default(Null);
            $table->text("keywords")->nullable()->default(Null);
            $table->string("title")->nullable()->default(Null);
            $table->text("description")->nullable()->default(Null);
            $table->string("fb")->nullable()->default(Null);
            $table->string("tw")->nullable()->default(Null);
            $table->string("ln")->nullable()->default(Null);
            $table->string("you")->nullable()->default(Null);
            $table->string("phone")->nullable()->default(Null);
            $table->string("address")->nullable()->default(Null);
            $table->string("tel")->nullable()->default(Null);
            $table->string("email")->nullable()->default(Null);
            $table->string("envelope")->nullable()->default(Null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
